var background_map;
var mid;

function unloaded() {
  console.log ('unloaded');
}
window.addEventListener('unload', unloaded, false);

$(document).ready(function() {
  try {
    $.console ('Validating user session');
    if (validate_session()) {
      window.addEventListener("load", loaded, false);
    } else {
      window.location.href = "login.html";
    }
  } catch (e) {
    log ("Home ready error: " + e);
    return false;
  }
});
function loaded() {
  $.console ('The window has loaded');
  $('#div_navbar').html(html_navbar);
  $('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });
  get_home_details();
}

function reload() {
  try {
    var head = document.getElementsByTagName("head")[0];
    if (head && head.innerHTML) {
      head.innerHTML += '<meta http-equiv="refresh" content="1">';
    }
  } catch (e) {
    console.log ('Reload error: ' + e);
  }
}

function get_home_details() {
  try {
    $.console ('Fetching the home data');
    
    $('#stat_input_start').prop('value', new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear());
    $('#stat_input_end').prop('value', new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear());
    
    var ajax = config.ajax;
    ajax.headers = { action: 'get_user_home' };
    ajax.error = function (xhr, status, error) {
      $.console ('get_home_details http (' + status + ') ' + error);
    };
    ajax.success = function (resp) {
      try {
        if (resp.error) {
          $.console ('get_home_details api error (' + resp.error + ') ' + resp);
        } else {
          $.console ('Building thumbnails');
          // create a thumbnail for each map
          var html = '';
          var end = '';
          var row = 0;
          
          for (var x = 0; x < resp.maps.length; x++) {
            if (row == 0) {
              html += '<div class="row">';
              end += '</div>';
            }
            
            var zoom = $.get_distance_between(resp.maps[x].start_latitude, resp.maps[x].start_longitude, resp.maps[x].end_latitude, resp.maps[x].end_longitude);
            if (zoom > 5000) zoom = 2;
            else if (zoom > 2000) zoom = 3;
            else if (zoom > 500) zoom = 4;
            else if (zoom > 100) zoom = 7;
            else zoom = 9;
            
            var mid_point = $.get_mid_point(resp.maps[x].start_latitude, resp.maps[x].start_longitude, resp.maps[x].end_latitude, resp.maps[x].end_longitude);
            var src = 'http://maps.googleapis.com/maps/api/staticmap?center=' + mid_point[0] + ',' + mid_point[1] + '&zoom=' + zoom + '&size=200x200';
            
            html += $.build_html ('div', { class: 'col-xs-6 col-sm-6 col-md-4' },
              $.build_html ('div', { class: 'panel panel-default' }, 
                $.build_html ('div', { class: 'panel-heading' }, resp.maps[x].name + $.build_html ('button', {
                    type: 'button',
                    class: 'btn btn-default btn-xs pull-right',
                    onclick: "window.location.href='editor.html?map_id=" + resp.maps[x].id + "';"
                  }, 'Edit')
                ) + $.build_html ('div', { class: 'panel-body' },
                  $.build_html ('div', { class: 'center-block' },
                    $.build_html ('a', { href: 'map.html?map_id=' + resp.maps[x].id },
                      $.build_html ('img', { src: src, class: 'img-thumbnail' })
                    )
                  )
                )
              )
            );
            
            if (row == 3) {
              row = 0;
              html += end;
              end = '';
            } else row++;
          } // END for
          html += '</div>';
          $('#output_map_list').html(html);
        }
      } catch (e) {
        $.console ('get_home_details success: (' + resp.error + ') ' + resp);
      }
    };
    $.ajax(ajax);
  } catch (e) {
    $.console ('get_home_details error: ' + e);
  }
}
