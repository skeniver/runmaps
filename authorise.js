$(document).ready(function() {
  try {
    // check for URL parameters
    if (validate_session()) {
      if ($.url_param('code') != 'null') {
        $.ajax({
          type: "GET",
          url: config.url_api,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('action', 'save_runkeeper_token');
            xhr.setRequestHeader('user_id', $.cookie(config.cookie).split(':')[0]);
            xhr.setRequestHeader('runkeeper_token', $.url_param('code'));
          },
          error: function (xml, text, status) {
            log ('authentication http error: ' + text);
          },
          success: function (data) {
            if (data.error) {
              log ('authentication data error: ' + data.error + ": " + data.description);
            } else {
              window.location.href = "home.html";
            }
          }
        });
      }
      
      var button_url = 'https://runkeeper.com/apps/authorize?client_id=7d87ecffed274bd280f08ad811814795&redirect_uri=';
      var this_url = $(location).attr('href');
      button_url += this_url.replace('authorise.html', 'cgi-bin/authorise.pl&response_type=code&state=');
      $('#button_runkeeper').html('<a href="' + button_url + '"><img src="http://static1.runkeeper.com/images/assets/connect-blue-white-200x38.png"/></a>');
    } else {
      window.location.href = "login.html";
    }
  } // main try block
  catch (e) {
    log ("Authorisation error: " + e);
  } //catch
});
