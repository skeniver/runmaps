#!/usr/bin/perl

use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use DBI;
use HTTP::Request;
use HTTP::Headers;
use LWP::UserAgent;
use JSON;
use DateTime;
use List::MoreUtils qw/ uniq /;
use Data::Dumper;
use Time::Piece;

use constant {
  URL => 'https://api.runkeeper.com',
  URL_PROFILE => '/profile',
  URL_SETTINGS => '/settings',
  URL_FITNESS_ACTIVITIES => '/fitnessActivities',
  URL_STRENGTH_TRAINING_ACTIVITIES => '/strengthTrainingActivities',
  URL_BACKGROUND_ACTIVITIES => '/backgroundActivities',
  URL_SLEEP => '/sleep'
};

my $json = qq{{"error":"0015 - script not run"}};
my $dbh;
my $cgi = CGI->new;

my $get = $cgi->param('get');
my $token;
my $debug = (defined $cgi->param('debug')) ? $cgi->param('debug') : 0;

my @param_array = ('map', 'elite');

if (defined $get) {
  $get = lc($get);
  if (grep $_ eq $get, @param_array) {
    
    if ($get eq 'map') {
      my $map = $cgi->param('map');
      if (defined $map) {
        $json = &get_monthly_data($map);
      } else {
        $json = qq{{"error":"0022 - map id is required (map=)"}};
      }
    }
    
    #####
    # Retrieve a users profile information
    # Currently only for the purposes of checking if they are elite
    elsif ($get eq 'elite') {
      my $user = $cgi->param('user');
      if (defined $user) {
        $json = &get_profile($user);
      } else {
        $json = qq{{"error":"0047 - user ID is required"}};
      }
    }
    
  } else {
    $json = qq{{"error":"0017 - unrecognised get parameter: $get"}};
  }  
} else { # GET parameter is not defined
  $json = qq{{"error":"0016 - No get parameter specified"}};
}

print $cgi->header(
  -type => "application/json",
  -charset => "utf-8"
);
print $json;
print "\n";


################
# RunKeeper subs
################
sub get_profile {
  my ($user) = @_;
  my $token = &get_runkeeper_token($user);
  my $url = URL . URL_PROFILE;
  
  my $header = HTTP::Headers->new;
  $header->header(
    "Host" => "api.runkeeper.com",
    "Authorization" => "Bearer $token",
    "Accept" => "application/vnd.com.runkeeper.Profile+json",
  );
  my $request = HTTP::Request->new("GET", $url, $header);
  my $ua = LWP::UserAgent->new;
  my $response = $ua->request($request);
  if ($response->is_success) {
    my $json_obj = new JSON;
    my $json = $json_obj->allow_nonref->utf8->relaxed->decode($response->decoded_content);
    return &set_elite($user, $json->{elite});
  } else {
    return qq{{"error":"0048 - " . $response->status_line}};
  }
}

sub get_monthly_data {
  my ($map) = @_;
  my $map_details = &get_map_details($map);
  my $output;
  my @activities = ();
  
  if (defined $map_details) {
    my ($day, $month, $year) = (localtime($map_details->{'StartDate'}))[3,4,5];
    $year += 1900;
    $month += 1;
    $month = ($month < 10) ? "0$month" : "$month";
    $day = ($day < 10) ? "0$day" : "$day";
    
    my $url_append = "/fitnessActivities?noEarlierThan=$year-$month-$day";
    my $breaker = 0;
    
    while (defined $url_append) {
      my $url = URL . $url_append;
      my $header = HTTP::Headers->new;
      $header->header(
        "Host" => "api.runkeeper.com",
        "Authorization" => "Bearer $map_details->{'RunKeeperToken'}",
        "Accept" => "application/vnd.com.runkeeper.FitnessActivityFeed+json"
      );
      my $request = HTTP::Request->new("GET", $url, $header);
      my $ua = LWP::UserAgent->new;
      my $response = $ua->request($request);
#print "url: " . $url_append . "\n";
      if ($response->is_success) {
        my $json_obj = new JSON;
        my $json = $json_obj->allow_nonref->utf8->relaxed->decode($response->decoded_content);
        $url_append = $json->{next};
#        $total_activities = $json->{size};
        
        foreach my $activity (@{$json->{items}}) {
          my $epoch = &convert_rk_date_to_epoch($activity->{start_time});
          my %act = (
            date=>$epoch,
            distance=>$activity->{total_distance},
            duration=>$activity->{duration},
            type=>$activity->{type},
          );
          push @activities, \%act;
#print "\n";
#print Dumper("ACT: " . \%act);
#print "\n";
        }
#print "No acts: " .@activities . "\n";
        
      } else { # $resp is not successful
        undef $url_append;
        my $error = $response->status_line;
        if ($response->status_line =~ /^401/) {
        # Error 401 Unauthorized, so the token is invalid
          $output = qq{{"error":"0042 - invalid RunKeeper token"}};
        } else {
          my $error = $response->status_line;
          $output = qq{{"error":"0021 - error retrieving user history: $error"}};
        }
      }
    } # end of the while loop
    
    if (defined $output) {
    } else {
      $output = &make_map_json($map_details, @activities);
    }
  } else {
    $output = qq{{"error":"0041 - Map ID $map doesn't exist"}};
  }
  
  return $output;
}

sub make_map_json {
  if (@_ >= 2) {
    my $total_distance = 0;
    my $total_duration = 0;
    my %month_distance = ();
    my %month_duration = ();
    my %month_activities = ();
    my %month_name = ();
    
    my ($map_details, @activities) = @_;
    my $month_index = &get_month_index(time());
    # Iterate over each activity and add it to the appropriate month hash
    # an activity on 20-05-2013 would get added to the month_activities{201305} hash
    foreach (@activities) {
      my %activity = %{$_};
      
      my $label_date = &get_month_label($activity{'date'});
      my $index_date = &get_month_index($activity{'date'});
      $month_distance{$index_date} += $activity{'distance'};
      $month_duration{$index_date} += $activity{'duration'};
      $month_name{$index_date} = $label_date;
      
      $month_activities{$index_date}{$activity{'date'}} = {
        'distance'=>$activity{'distance'},
        'duration'=>$activity{'duration'},
        'type'=>$activity{'type'}        
      };
      
      $total_distance += $activity{'distance'};
      $total_duration += $activity{'duration'};
    }
    
    my %months = ();
    
    foreach my $key (keys %month_distance) {
      $months{$key} = {
        'name'=>$month_name{$key},
        'distance'=>$month_distance{$key},
        'duration'=>$month_duration{$key},
        'activities'=>$month_activities{$key}
      };
    }
    
    my $js = JSON::XS->new->utf8;
    my $output = $js->encode({
      "success"=>"data for map id " . $map_details->{'id'},
      "total_distance"=>"$total_distance",
      "total_duration"=>"$total_duration",
      "months"=>\%months
    });
    
    return $output;
  } else {
    return qq{{"error":"0041 - No map data available"}};
  }
}





##############
# DB subs
##############
sub db_connect {
  $dbh = DBI->connect(
    "DBI:mysql:database=runmaps;host=54.243.159.181;port=3306",
    "work_scripts",
    "BVPZbmSywQejvvjB")
  or die $DBI::errstr;
}

sub set_elite {
  my ($user, $elite) = @_;
  $elite = ($elite eq 'true') ? 1 : 0;
  my $output = ($elite == 1) ? "User is elite" : "User is free";
  db_connect();
#  if ($elite eq 'true') {
#    $elite = 1;
#  } else {
#    $elite = 0;
#  }
#  print "Elite: " . $elite . "\n";
  my $sql = q{
    UPDATE tblUsers SET Elite=? WHERE id=?
  };
  my $rows = $dbh->do($sql, undef, $elite, $user);
  $DBI::err && die $DBI::errstr;
  if (defined $rows && $rows == 1) {
    return qq{{"success":"$output"}};
  } else {
    if (defined $rows) {
      return qq{{"error":"0049 - $rows were updated"}};
    } else {
      return qq{{"error":"0048 - SQL error:\n$sql"}};
    }
  }
}

sub get_runkeeper_token {
  my ($user) = @_;
  db_connect();
  my $sql = qq{
    SELECT RunKeeperToken FROM tblUsers WHERE id=?
  };
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($user)
    or die $dbh->errstr;
  my ($token) = $sth->fetchrow_array;
  
  return $token;
}

sub get_map_details {
  my ($map) = @_;
  db_connect();
  my $sql = qq{
    SELECT
      tblMaps.id,tblMaps.UserId,tblUsers.RunKeeperToken,tblMaps.Name,
      tblMaps.StartDate,tblMaps.StartLatitude,tblMaps.StartLongitude,
      tblMaps.EndLatitude,tblMaps.EndLongitude,tblMaps.Metric,tblMaps.TravelMode,
      tblMaps.DateCreated,tblMaps.DateDeleted
    FROM tblMaps
    JOIN tblUsers ON tblMaps.UserId = tblUsers.id
    WHERE tblMaps.id=?
  };
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($map)
    or die $dbh->errstr;
#  my ($mid, $uid, $name, $sd, $slat, $slong, $elat, $elong, $metric, $tm) = $sth->fetchrow_array;
  my $map_details = $sth->fetchrow_hashref;
  
  return $map_details;
}


##########
# Other subs
##########

sub convert_rk_date_to_epoch {
  my %months = (
    "Jan" => 1,
    "Feb" => 2,
    "Mar" => 3,
    "Apr" => 4,
    "May" => 5,
    "Jun" => 6,
    "Jul" => 7,
    "Aug" => 8,
    "Sep" => 9,
    "Oct" => 10,
    "Nov" => 11,
    "Dec" => 12
  );
  my ($str_date) = @_;
  if (defined $str_date) {
    my ($dow, $day, $str_month, $year, $time) = split(' ', $str_date);
    my ($hour, $min, $sec) = split(':', $time);
    my $month = $months{$str_month};
    my $datetime = DateTime->new(
      year => $year,
      month => $month,
      day => $day,
      hour => $hour,
      minute => $min,
      second => $sec
    );
    
#    print "RK DATE: $str_date -> $day-$month-$year\n";
    
    return $datetime->epoch();
  } else {
    return undef;
  }
}

sub get_month_index {
  my ($epoch) = @_;
  my $date = DateTime->from_epoch(epoch=>$epoch);
  
  my $mon = ($date->month() < 10) ? "0" . $date->month : $date->month;
  my $year = $date->year();
  return "$year$mon";
}

sub get_month_label {
  my ($epoch) = @_;
  my $date = DateTime->from_epoch(epoch=>$epoch);
  
  return $date->month_abbr() . ' ' . $date->year();
}
