#!/usr/bin/perl
use CGI;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use DBI;
use strict;
use warnings;
use JSON;
use DateTime;
use Email::MIME;
use Email::Valid;
use Email::Sender::Simple qw(sendmail);

# read the CGI params
my $dbh;
my $cgi = CGI->new;
my $json = qq{{"error":"test"}};

my $action = $cgi->param('action');

my $httppass = $cgi->param('password');

if (defined $action) { # There needs to be an action parameter to know what to do
  $action = lc($action);
  
  # Session key validation
  # a user id and session key are supplied and then checked to make sure they are valid
  if ($action eq 'validate') { # Session key verification
    my $userid = $cgi->param('user');
    my $session_key = $cgi->param('sessionkey');
    
    if (defined $userid && defined $session_key) { # need to have a user id and session key to perform verification
      $json = &is_session_key_valid($userid, $session_key);
    } else {
      $json = qq{{"error":"0007 - validation requires a user id and session key"}}
    }
  }
  
  # User log in request
  # an email and password are supplied and must be verified
  elsif ($action eq 'login') { # user log in
    my $email = $cgi->param('email');
    my $password = $cgi->param('password');
    
    if (defined $email && defined $password) { # need an email and password to log in
      $json = &log_user_in(lc($email), $password);
    }    
    else {
      $json = qq{{"error":"0008 - log in requires an email and password"}};
    }
  }
  
  # User registration request
  # an email address and password are supplied and then added to the DB
  # a user id and session key are returned
  elsif ($action eq 'register') {
    my $email = $cgi->param('email');
    my $password = $cgi->param('password');
    
    if (defined $email && defined $password) {
      $json = &register_user(lc($email), $password);
    } else {
      $json = qq{{"error":"0009 - registration requires an email and password"}};
    }
  }
  
  
  # Add a users RunKeeper Token
  # a user has authenticated RunKeeper and we received an access token
  # store this in the database
  elsif ($action eq 'savetoken') {
    my $userid = $cgi->param('user');
    my $token = $cgi->param('token');
    
    if (defined $userid && defined $token) {
      $json = &save_runkeeper_token($userid, $token);
    } else {
      $json = qq{{"error":"0011: a user id and token are required"}};
    }
  }
  
  # retrieve a list of the users maps
  elsif ($action eq 'listmaps') {
    my $user_id = $cgi->param('user');
    
    if (defined $user_id) {
      $json = &get_users_maps($user_id);
    } else {
      $json = qq{{"error":"0024 - a user id is required"}};
    }
  }
  
  # retrieve a specific map
  elsif ($action eq 'getmap') {
    my $map_id = $cgi->param('map');
    if (defined $map_id) {
      $json = &get_map($map_id);
    } else {
      $json = qq{{"error":"0025 - no map id specified"}};
    }
  }
  
  # save a new map
  elsif ($action eq 'savemap') {
    my $user_id = $cgi->param('user');
    my $name = $cgi->param('name');
    my $slat = $cgi->param('slat');
    my $slong = $cgi->param('slong');
    my $elat = $cgi->param('elat');
    my $elong = $cgi->param('elong');
    my $travel = $cgi->param('travel');
    my $date = $cgi->param('date');
    my $metric = $cgi->param('metric');
    my $session_key = $cgi->param('session');
    
    if (defined $user_id) { # map saving needs a user id
      if (defined $session_key) { # and a session key
        if (defined $name && defined $slat && defined $slong && defined $elat && defined $elong
          && defined $travel && defined $date && defined $metric) {
            $json = &save_map($user_id, $session_key, $name, $slat, $slong, $elat, $elong, $travel, $date, $metric);
        } else { # some of the other variables are missing
          { # no warnings for this code
            no warnings 'uninitialized';
            $json = qq{{
              "error":"0028 - missing parameters:name: ", defined $name, "; start lat: ", defined $slat, "; start long: \$slong; end lat: \$elat; end long: $elong; travel: $travel; date: $date; metric: $metric"
            }};
          }
        }
      } else { # there is no session key
        $json = qq{{"error":"0034 - session key required"}};
      }
    } else { # there is no user id
      $json = qq{{"error":"0027 - user id not specified"}};
    }    
  }
  
  # edit an existing map
  elsif ($action eq 'editmap') {
    my $map_id = $cgi->param('map');
    my $name = $cgi->param('name');
    my $slat = $cgi->param('slat');
    my $slong = $cgi->param('slong');
    my $elat = $cgi->param('elat');
    my $elong = $cgi->param('elong');
    my $travel = $cgi->param('travel');
    my $date = $cgi->param('date');
    my $metric = $cgi->param('metric');

    if (defined $map_id) {
      if (defined $name && defined $slat && defined $slong && defined $elat && defined $elong
        && defined $travel && defined $date && defined $metric) {
          $json = &edit_map($map_id, $name, $slat, $slong, $elat, $elong, $travel, $date, $metric);
        } else { # some of the other variables are missing
          $json = qq{{
            "error":"0031 - missing parameters:name: $name; start lat: $slat; start long: $slong; end lat: $elat; end long: $elong; travel: $travel; date: $date; metric: $metric"
          }};
        }
    } else { # there is no user id
      $json = qq{{"error":"0033 - map id not specified"}};
    }
  }
  
  # Verify that a username is unique
  elsif ($action eq 'username') {
    my $username = $cgi->param('username');
    if (defined $username) {
      $json = &verify_username(lc($username));
    } else {
      $json = qq{{"error":"0038 - username is required"}};
    }
  }

  # Log a user out by changing the session key
  elsif ($action eq 'logout') {
    my $user = $cgi->param('user');
    if (defined $user) {
      $json = &log_user_out($user);
    } else {
      $json = qq{{"error":"0030 - user id required"}};
    }
  }
  
  # Send a contact email
  elsif ($action eq 'contact') {
    my $email = $cgi->param('email');
    if (defined $email) {
      if (Email::Valid->address($email)) {
        my $message = $cgi->param('content');
        my $username = $cgi->param('username');
        if (defined $message) {
          $json = &send_email($email, $message, $username);
        } else {
          $json = qq{{"error":"0044 - some message content is required"}};
        }
      } else {
        $json = qq{{"error":"0046 - not a valid email address"}};
      }
    } else {
      $json = qq{{"error":"0043 - an email address is required"}};
    }
  }
  
  # toggle the location sharing setting
  elsif ($action eq 'location') {
    my $user_id = $cgi->param('user');
    my $enabled = $cgi->param('enabled');
    if (defined $user_id) {
      if (defined $enabled) {
        $json = &enable_location_sharing($user_id, $enabled);
      } else {
        $json = qq{{"success":"0056 - enabled must have a value"}};
      }
    } else {
      $json = qq{{"error":"0054 - user ID required"}};
    }
  }
  
  # Make a user an OSM KEY
  elsif ($action eq 'newkey') {
    my $user_id = $cgi->param('user');
    if (defined $user_id) {
      $json = &new_osm_key($user_id);
    } else {
      $json = qq{{"error":"0050 - user ID is required"}};
    }
  }
  
  # Set the user a new random URL
  elsif ($action eq 'newurl') {
    my $user_id = $cgi->param('user');
    if (defined $user_id) {
      $json = &new_sharing_url($user_id);
    } else {
      $json = qq{{"error":"0052 - user ID is required"}};
    }
  }
  
  # Read in a users location data
  elsif ($action eq 'tracking') {
    my $user_key = $cgi->param('osm');
    if (defined $user_key) {
      my $lat = $cgi->param('lat');
      my $lon = $cgi->param('lon');
      my $time = $cgi->param('time');
      my $hdop = $cgi->param('hdop');
      my $alt = $cgi->param('alt');
      my $speed = $cgi->param('speed');
      if (defined $lat && defined $lon && defined $time && defined $hdop && defined $alt && defined $speed) {
        $json = &save_current_location($user_key, $lat, $lon, $time, $hdop, $alt, $speed);
      } else {
        $json = qq{{"error":"0059 - lat, lon, time, hdop, alt and speed are required"}}
      }
    } else {
      $json = qq{{"error":"0058 - OSM Key is required"}};
    }
  }
  
  # Send a users location data
  elsif ($action eq 'find') {
    my $key = $cgi->param('url');
    if (defined $key) {
      $json = &get_user_location($key);
    } else {
      $json = qq{{"error":"0060 - key is required"}};
    }
  }
  
  # Make a username the tracking key
  elsif ($action eq 'userurl') {
    my $userid = $cgi->param('user');
    my $enabled = $cgi->param('value');
    if (defined $userid) {
      if (defined $enabled) {
        if ($enabled eq '1' || $enabled eq '0') {
          $json = &set_username_url($userid, $enabled);
        } else {
          $json = qq{{"error":"0064 - value is incorrect"}};
        }
      } else {
        $json = qq{{"error":"0063 - value is required"}};
      }
    } else {
      $json = qq{{"error":"0065 - user id is required"}};
    }
  }
  
  # Save a new username and password
  elsif ($action eq 'savesettings') {
    my $userid = $cgi->param('user');
    my $username = $cgi->param('un');
    my $password = $cgi->param('pw');
    if (defined $userid) {
      if (defined $username || defined $password) {
        $json = &save_user_details($userid, $username, $password);
      } else {
        $json = qq{{"error":"0067 - a username or password is required"}};
      }
    } else {
      $json = qq{{"error":"0066 - user is is required"}};
    }
  }
  

  ## A test action to do what ever I need it to
  elsif ($action eq 'testing') {
    my $time = time;
    $json = qq{{"time":"$time"}};
  }
  
  
  ##############
  ## The action parameter is unrecognised
  else {
    $json = qq{{"error":"0006 - unrecognised action: $action"}};
  }
} # end of action if
else {
  $json = qq{{"error":"0005 - No action parameter specified"}};
}

# return JSON string
print $cgi->header(
  -type => "application/json",
  -charset => "utf-8"
);
print $json;
print "\n";


#############
# Send email
#############
sub send_email {
  my ($from, $content, $username) = @_;
  if (defined $username) {
    $username = "RunMaps contact from: $username";
  } else {
    $username = "RunMaps contact";
  }
  
  my $sendmail = '/usr/sbin/sendmail';
  open(MAIL, "|$sendmail -t")
    or return qq{{"error":"0045 - couldn't open file"}};
  print MAIL "From: $from\n";
  print MAIL "To: skeniver+runmaps\@gmail.com\n";
  print MAIL "Subject: $username\n";
  print MAIL "$content\n";
  close(MAIL);
  
  return qq{{"success":"Message sent"}};
}




##############
# DB subs
##############
sub db_connect {
  $dbh = DBI->connect(
    "DBI:mysql:database=runmaps;host=54.243.159.181;port=3306",
    "work_scripts",
    "BVPZbmSywQejvvjB")
    or die $DBI::errstr;
}

sub save_user_details {
  my ($userid, $username, $password) = @_;
  
  my $sql = "
    UPDATE tblUsers SET
  ";
  my @updates;
  if (defined $username) {
    if (defined $password) {
      $sql .= " Email=?, Password=? ";
      push @updates, $username;
      push @updates, $password;
    } else {
      $sql .= " Email=? ";
      push @updates, $username;
    }
  } else {
    $sql .= " Password=? ";
    push @updates, $password;
  }
  $sql .= " WHERE id=?";
  push @updates, $userid;
  
  db_connect();
  my $result = $dbh->do($sql, undef, @updates) or die $dbh->errstr;
  $dbh->disconnect;
  
  return qq{{"success":"$result rows updated"}};
}

sub set_username_url {
  my ($userid, $enabled) = @_;
  db_connect();
  
  my $sql = "UPDATE tblPosition set UsernameUrl=? WHERE UserId=?";
  my $sth = $dbh->prepare($sql) or die $dbh->errstr;
  $sth->execute($enabled, $userid) or die $dbh->errstr;
  $sth->finish;
  $dbh->disconnect;
  
  return qq{{"success":"Tracking updated ($userid;$enabled)"}};
}

sub get_user_location {
  my ($key) = @_;
  my $response;
  db_connect();
  
  my $sql = "
    SELECT
      pos.UserId,pos.Enabled,pos.Latitude,pos.Longitude,pos.Time,pos.HDOP,pos.Altitude,pos.Speed
    FROM tblPosition pos
    JOIN tblUsers users ON users.id = pos.UserId
    WHERE (pos.Url=? AND pos.UsernameUrl=0) OR (users.Email=? AND pos.UsernameUrl=1)
  ";
  my $sth = $dbh->prepare($sql) or die $dbh->errstr;
  $sth->execute($key, $key) or die $dbh->errstr;
  my ($userid, $enabled, $lat, $lon, $time, $hdop, $alt, $speed) = $sth->fetchrow_array;
  $sth->finish;
  
  if (defined $userid) {
    if ($enabled == 1) {
      $response = qq{{
        "success":"User location retrieved",
        "latitude":"$lat",
        "longitude":"$lon",
        "time":"$time",
        "hdop":"$hdop",
        "altitude":"$alt",
        "speed":"$speed"
      }};
    } else {
      $response = qq{{"error":"0062 - tracking is not enabled"}};
    }
  } else {
    $response = qq{{"error":"0061 - tracking key doesn't exist"}};
  }
  
  $dbh->disconnect;
  
  return $response;
}

sub save_current_location {
  my ($key, $lat, $lon, $time, $hdop, $alt, $speed) = @_;
  db_connect();
  
  my $sql = "
    UPDATE
      tblPosition
    SET
      Latitude=?,Longitude=?,Time=?,HDOP=?,Altitude=?,Speed=?
    WHERE OsmKey=?
  ";
  my $sth = $dbh->prepare($sql) or die $dbh->errstr;
  $sth->execute($lat, $lon, $time, $hdop, $alt, $speed, $key) or die $dbh->errstr;
  $sth->finish;
  
  $dbh->disconnect;
  return qq{{"success":"User location saved"}};
}

sub enable_location_sharing {
  my ($uid, $enabled) = @_;
  if (defined $uid) {
    db_connect();
    
    my $url = &generate_new_url();
    my $key = &generate_osm_key();
    my $sql = "";
    my $response = "";
    
    if ($enabled == 0) {
      $sql = "
        INSERT INTO tblPosition
        (UserId, Enabled, OsmKey, UsernameUrl, Url) VALUES (?, 0, ?, 0, ?)
        ON DUPLICATE KEY UPDATE
        Enabled = 0
      ";
      $response = "Location sharing disabled";
    } elsif ($enabled == 1) {
      $sql = "
        INSERT INTO tblPosition
        (UserId, Enabled, OsmKey, UsernameUrl, Url) VALUES (?, 1, ?, 0, ?)
        ON DUPLICATE KEY UPDATE
        Enabled = 1
      ";
      $response = "Location sharing enabled";
    } else {
      return qq{{"error":"0057 - enabled must be 0/1"}};
    }
    
    my $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute($uid, $key, $url) or die $dbh->errstr;
    $sth->finish;
    
    $sql = "SELECT Enabled, OsmKey, UsernameUrl, Url FROM tblPosition WHERE UserId=?";
    $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute($uid) or die $dbh->errstr;
    my ($enabled, $osmkey, $userurl, $shareurl) = $sth->fetchrow_array;
    $sth->finish;
    
    $dbh->disconnect;
    
    return qq{{
      "success":"$response",
      "enabled":"$enabled",
      "key":"$osmkey",
      "userurl":"$userurl",
      "url":"$shareurl"
    }};
  } else {
    return qq{{"error":"0055 - user ID required"}};
  }
}

sub new_sharing_url {
  my ($uid) = @_;
  if (defined $uid) {
    db_connect();
    
    my $url = &generate_new_url();
    my $sql = "INSERT INTO tblPosition (UserID, Url, UsernameUrl) VALUES (?,?,0) ON DUPLICATE KEY UPDATE Url=?, UsernameUrl=0";
    my $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute($uid, $url, $url);
    $sth->finish;
    $dbh->disconnect;
    
    return qq{{"success":"$url"}};
  } else {
    return qq{{"error":"0052 - no user ID"}};
  }
}

sub new_osm_key {
  my ($uid) = @_;
  if (defined $uid) {
    db_connect();
    
    my $key = &generate_osm_key();
    my $sql = "INSERT INTO tblPosition (UserId, OsmKey) VALUES (?, ?) ON DUPLICATE KEY UPDATE OsmKey=?";
    my $sth = $dbh->prepare($sql) or die $dbh->errstr;
    $sth->execute($uid, $key, $key) or die $dbh->errstr;
    $sth->finish;
    $dbh->disconnect();
    
    return qq{{"success":"$key"}};
  } else {
    return qq{{"error":"0051 - no user ID"}};
  }
}

sub verify_username {
  my ($username) = @_;
  if (defined $username) {
    db_connect();
    my $sql = qq{SELECT COUNT(id) FROM tblUsers WHERE Email=?};
    my $sth = $dbh->prepare($sql)
      or die $dbh->errstr;
    $sth->execute($username)
      or die $dbh->errstr;
    my ($id) = $sth->fetchrow_array;
    if ($id == 0) {
      return qq{{"success":"username $username is unique"}};
    } else {
      return qq{{"error":"0037 - user id is taken"}};
    }
  } else {
    return qq{{"error":"0038 - username required"}};
  }
}

sub log_user_out {
  my ($user) = @_;
  if (defined $user) {
    db_connect();
    my $sql = qq{UPDATE tblUsers SET SessionKey=null WHERE id=?};
#    my $sth = $dbh->prepare($sql)
#      or die $dbh->errstr;
    my $rows = $dbh->do($sql, undef, $user)
      or die $dbh->errstr;
    if ($rows == 1) {
      return qq{{"success":"user $user logged out"}};
    } else {
      return qq{{"error":"0031 - $rows have been changed}};
    }
  } else {
    return qq{{"error":"0030 - user id not specified"}};
  }
}

sub edit_map {
  my ($map_id, $name, $slat, $slong, $elat, $elong, $travel, $date, $metric) = @_;
  my $sql = qq{UPDATE tblMaps SET
    Name=?,StartDate=?,StartLatitude=?,StartLongitude=?,EndLatitude=?,EndLongitude=?,Metric=?,TravelMode=?,DateEdited=?
    WHERE id=?
  };
  db_connect();
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  my $rows = $dbh->do($sql, undef, $name, date_to_epoch($date), $slat, $slong, $elat, $elong, $metric, $travel, time, $map_id);
  $DBI::err && die $DBI::errstr;
  if (defined $rows && $rows == 1) {
    return qq{{"success":"$map_id"}};
  } else {
    if (defined $rows) {
      return qq{{"error":"0032 - $rows were updated"}};
    } else {
      return qq{{"error":"0034 - SQL error:\n$sql"}};
    }
  }
}

sub save_map {
  my ($user_id, $session_key, $name, $slat, $slong, $elat, $elong, $travel, $date, $metric) = @_;
  
  db_connect();
  my $sql = qq{SELECT COUNT(id) FROM tblUsers WHERE id=? AND SessionKey=?};
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($user_id, $session_key)
    or die $dbh->errstr;
  my ($count) = $sth->fetchrow_array;
  
  if ($count == 1) {
    $sql = qq{INSERT INTO tblMaps 
      (UserId,Name,StartDate,StartLatitude,StartLongitude,EndLatitude,EndLongitude,Metric,TravelMode,DateCreated)
      VALUES
      (?,?,?,?,?,?,?,?,?,?)
    };
    $sth = $dbh->prepare($sql)
      or die $dbh->errstr;
    $sth->execute($user_id, $name, date_to_epoch($date), $slat, $slong, $elat, $elong, $metric, $travel, time())
      or die $dbh->errstr;
    my $map_id = $dbh->{mysql_insertid};
  
    if (defined $map_id) {
      return qq{{"success":"$map_id"}};
    } else {
      return qq{{"error":"0029 - Map not saved"}};
    }
  } elsif ($count > 1) {
    return qq{{"error":"0035 - Map not saved"}};
  } else {
    return qq{{"error":"0036 - Map not saved"}};
  }
}

sub get_map {
  my ($map_id) = @_;
  db_connect();
  my $sql = qq{SELECT id,UserId,Name,StartDate,StartLatitude,StartLongitude,EndLatitude,EndLongitude,Metric,TravelMode,DateCreated FROM tblMaps WHERE id=?};
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($map_id)
    or die $dbh->errstr;
  my ($id, $user, $name, $sdate, $slat, $slong, $elat, $elong, $metric, $travel, $datec) = $sth->fetchrow_array;
  
  if (defined $id) {
    $sql = qq{UPDATE tblMaps SET DateViewed=? WHERE id=?};
    my $rows = $dbh->do($sql, undef, time, $id);
    $DBI::err && die $DBI::errstr;
    my $j = JSON::XS->new->utf8;
    return $j->encode({
      "success"=>"map with id $id returned",
      "map"=>{
        "id"=>"$id",
        "user"=>"$user",
        "name"=>"$name",
        "start_latitude"=>"$slat",
        "start_longitude"=>"$slong",
        "end_latitude"=>"$elat",
        "end_longitude"=>"$elong",
        "metric"=>"$metric",
        "travel_mode"=>"$travel",
        "date_created"=>"$datec",
        "start_date"=>"$sdate"
      }
    });
  } else {
    return qq{{"error":"0026 - no such map id"}};
  }
}

sub get_users_maps {
  my ($user_id) = @_;
  db_connect();
  my $sql = qq{SELECT id,Name,DateCreated FROM tblMaps WHERE UserId=?};
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($user_id)
    or die $dbh->errstr;
  
  my $maps = [];
  for (my $x = 0; $x < $sth->rows; $x++) {
    my ($id, $name, $date) = $sth->fetchrow_array();
    my $map = {
      "id"=>"$id",
      "name"=>"$name",
      "date_created"=>"$date"
    };
    push $maps, $map;
  }
  my $j = JSON::XS->new->utf8;
  my $output = $j->encode({
    "success"=>"list of users maps",
    "maps"=>$maps
  });
  
#  my $output = qq{{
#    "success":"list of users maps"
#    "maps":@maps
#  }};
  
  return $output;
}

sub is_session_key_valid {
  my ($userid, $session_key) = @_;
  db_connect();
  my $sql = "
    SELECT
      user.id, user.Email, user.SessionKey, user.RunKeeperToken, user.Elite, position.Enabled, position.OsmKey, position.UsernameUrl, position.Url
    FROM
      tblUsers user
      LEFT OUTER JOIN tblPosition position ON user.id = position.UserId
    WHERE
      user.id = ?
  ";
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($userid)
    or die $dbh->errstr;
  my ($uid, $username, $db_session_key, $token, $elite, $p_enabled, $osm_key, $userurl, $url) = $sth->fetchrow_array;
  if ($db_session_key eq $session_key) {
    return qq{{
      "success":"the user is logged in",
      "userid":"$userid",
      "username":"$username",
      "sessionkey":"$session_key",
      "token":"$token",
      "elite":"$elite",
      "location_enabled":"$p_enabled",
      "osm_key":"$osm_key",
      "url_username":"$userurl",
      "url":"$url"
    }};
  } else {
#    return qq{{"error":"0010 - the user isn't logged in: DB: $db_session_key; $session_key"}};
    return qq{{"error":"0010 - the user isn't logged in"}};
  }
}

sub log_user_in {
  my ($email, $password) = @_;
  db_connect();
  my $sql = qq{SELECT id,RunKeeperToken,CanLogin FROM tblUsers WHERE email=? AND password=?};
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($email, $password)
    or die $dbh->errstr;
  my ($userid, $token, $allow) = $sth->fetchrow_array;
  
  if (defined $userid) {
    if (defined $allow && $allow == 1) {
      my $session_key = generate_session_key();
      my $sql = qq{UPDATE tblUsers SET SessionKey=?,DateLastLogin=? WHERE id=?};
      my $sth = $dbh->prepare($sql)
        or die $dbh->errstr;
      $sth->execute($session_key, time, $userid)
        or die $dbh->errstr;
      if (defined $token) {
        return qq{{
          "success":"log in is succcessful",
          "userid":"$userid",
          "sessionkey":"$session_key",
          "token":"$token"
        }};
      } else { # User has no RunKeeper token
        return qq{{
          "success":"log in is succcessful",
          "userid":"$userid",
          "sessionkey":"$session_key"
        }};
      }
    } else { # User exists but is denied log in
      return qq{{"error":"0004 - Login not allowed"}};
    }
  } else { # User has entered incorrect password or email
    return qq{{"error":"0001 - incorrect credentials"}};
  }
}

sub register_user {
  my ($email, $password) = @_;
  db_connect();
  my $sql = qq{SELECT id FROM tblUsers WHERE Email=?};
  my $sth = $dbh->prepare($sql)
    or die $dbh->errstr;
  $sth->execute($email)
    or die $dbh->errstr;
  my ($userid) = $sth->fetchrow_array;

  if ($userid) { # this email address already exists
    return qq{{"error":"0003 - duplicate email"}};
  } else {
    my $session_key = generate_session_key();

    $sql = qq{INSERT INTO tblUsers (Email,Password,SessionKey,CanLogin,DateRegistered) VALUES (?,?,?,1,?)};
    $sth = $dbh->prepare($sql)
      or die $dbh->errstr;
    $sth->execute($email, $password, $session_key, time)
      or die $dbh->errstr;
    my $userid = $dbh->{mysql_insertid};

    if (defined $userid) {
      return qq{{
        "success":"user created",
        "userid":"$userid",
        "sessionkey":"$session_key"
      }};
    } else {
      return qq{{"error":"0002 user not added to database"}};
    }
  }
}

sub save_runkeeper_token {
  my ($userid, $token) = @_;
  
  if ($userid && $token) {
    if ($token ne 'null') {
      db_connect();
      my $sql = qq{UPDATE tblUsers SET RunKeeperToken=? WHERE id=?};
      my $sth = $dbh->prepare($sql)
        or die $dbh->errstr;
      my $rows = $dbh->do($sql, undef, $token, $userid);
      $DBI::err && die $DBI::errstr;
      
      if ($rows == 1) {
        return qq{{"success":"token $token added for user id $userid; rows: $rows"}};
      } else {
        return qq{{"error":"0012 - incorrect number of rows affected: $rows"}};
      }
    } else {
      return qq{{"error":"0013 - token cannot be null"}};
    }
  } else {
    return qq{{"error":"0011 - user id and token required"}};
  }
}




###############
#  Other subs #
###############
sub generate_session_key {
  my @chars = ('a'..'z', 'A'..'Z', '0'..'9');
  my $key;
  my $result;
  my $breaker = 0;

  do {
    $breaker++;
    if ($breaker == 100) {
      die "GENERATE SESSION KEY BREAK";
    }

    $key = '';
    foreach (1..15) {
      $key .= $chars[rand @chars];
    }

    my $sql = qq{SELECT id FROM tblUsers WHERE SessionKey=?};
    my $st = $dbh->prepare($sql)
      or die $dbh->errstr;
    $st->execute($key)
      or die $dbh->errstr;
    ($result) = $st->fetchrow_array;
  } until (!$result);

  return $key;
}

sub generate_new_url {
  my @chars = ('a'..'z', 'A'..'Z', '0'..'9');
  my $key;
  my $result;
  my $breaker = 0;
  
  do {
    $breaker++;
    if ($breaker == 100) {
      die "GENERATE URL BREAK";
    }
    
    $key = '';
    foreach (1..19) {
      $key .= $chars[rand @chars];
    }
    
    my $sql = "SELECT id FROM tblPosition WHERE Url=?";
    my $st = $dbh->prepare($sql) or die $dbh->errstr;
    $st->execute($key) or die $dbh->errstr;
    ($result) = $st->fetchrow_array;
  } until (!$result);
  
  return $key;

}

sub generate_osm_key {
  my @chars = ('a'..'z', 'A'..'Z', '0'..'9');
  my $key;
  my $result;
  my $breaker = 0;
  
  do {
    $breaker++;
    if ($breaker == 100) {
      die "GENERATE OSM KEY BREAK";
    }
    
    $key = '';
    foreach (1..19) {
      $key .= $chars[rand @chars];
    }
    
    my $sql = "SELECT id FROM tblPosition WHERE OsmKey=?";
    my $st = $dbh->prepare($sql) or die $dbh->errstr;
    $st->execute($key) or die $dbh->errstr;
    ($result) = $st->fetchrow_array;
  } until (!$result);
  
  return $key;
}

sub date_to_epoch {
  my ($date) = @_;
  my ($day, $month, $year) = split('/', $date);
  
  my $epoch = DateTime->new(
    year => $year,
    month => $month,
    day => $day,
    hour => 0,
    minute => 0,
    second => 1,
  );
  
  return $epoch->epoch;
}
