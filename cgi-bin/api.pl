#!/usr/bin/perl
use CGI;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use DBI;
use strict;
use warnings;
use JSON;
use DateTime;
use Email::MIME;
use Email::Valid;
use Email::Sender::Simple qw(sendmail);
use HTTP::Request;
use LWP::UserAgent;
use List::Util 'max';

use constant {
  SESSION_KEY_LENGTH => 20,
  OSM_KEY_LENGTH => 15,
  
  URL => 'https://api.runkeeper.com',
  URL_PROFILE => '/profile',
  URL_SETTINGS => '/settings',
  URL_FITNESS_ACTIVITIES => '/fitnessActivities',
  URL_STRENGTH_TRAINING_ACTIVITIES => '/strengthTrainingActivities',
  URL_BACKGROUND_ACTIVITIES => '/backgroundActivities',
  URL_SLEEP => '/sleep',
};

my $dbh;
my $cgi = CGI->new;
my %headers = map { $_ => $cgi->http($_) } $cgi->http();

my @cookie;
if ($headers{"HTTP_COOKIE"} =~ /runmaps/) {
  @cookie = split(';', $headers{"HTTP_COOKIE"});
  my $cook = '';
  foreach (@cookie) {
    if ($_ =~ /runmaps/) {
      $cook = $_;
    }
  }
  @cookie = split('=', $cook);
  @cookie = split('%3A', $cookie[1]);
}

my $json = JSON::XS->new->utf8;
my $output = $json->encode({
  "error" => "0001",
  "description" => "nothing happened",
  "headers" => \%headers,
});

my $action;

## Check first for a tracking action in the URL parameters
## If not, use the action header

if (lc($cgi->param('action')) eq 'tracking') {
  $output = &save_current_location();
} else {

## An action header is always required
  if (exists $headers{"HTTP_ACTION"}) {
    $action = lc($headers{"HTTP_ACTION"});
    
## Username is always lowercase
    if (exists $headers{"HTTP_USERNAME"}) {
      $headers{"HTTP_USERNAME"} = lc($headers{"HTTP_USERNAME"});
    }
    
## Check if the action is one that requires a valid session
## - If not, run it
## - If it does, check the session
    if ($action eq 'login') {
      if ((exists $headers{"HTTP_USERNAME"} || exists $headers{"HTTP_EMAIL"}) && exists $headers{"HTTP_PASSWORD"}) {
        $output = &log_user_in();
      } else {
        $output = $json->encode({
          "error" => "0004",
          "description" => "a username or email and password are required",
        });
      }
    }
    
    elsif ($action eq 'register') {
      if ($headers{"HTTP_USERNAME"} && $headers{"HTTP_PASSWORD"}) {
        $output = &register_user($headers{"HTTP_USERNAME"}, $headers{"HTTP_PASSWORD"});
      } else {
        $output = $json->encode({
          "error" => "0004",
          "description" => "a username or email and password are required",
        });
      }
    }
    
    elsif ($action eq 'verify_username') {
      if ($headers{"HTTP_USERNAME"}) {
        $output = &verify_username($headers{"HTTP_USERNAME"});
      } else {
        $output = $json->encode({
          "error" => "0006",
          "description" => "a username is required"
        });
      }
    }
    
    elsif ($action eq 'get_map') {
      if (exists $headers{"HTTP_MAP_ID"}) {
        $output = &get_map();
      } else {
        $output = $json->encode({
          "error" => "0008",
          "description" => "a map ID is required"
        });
      }
    }
    
    elsif ($action eq 'get_runkeeper_data') {
      if (exists $headers{"HTTP_MAP_ID"}) {
        $output = &get_runkeeper_data();
      } else {
        $output = $json->encode({
          "error" => "0008",
          "description" => "a map ID is required",
        });
      }
    }
    
    elsif ($action eq 'get_location') {
      if (exists $headers{"HTTP_USER_ID"}) {
        $output = &get_location();
      } else {
        $output = $json->encode({
          "error" => "0012",
          "description" => "a user ID is required"
        });
      }
    }
    
    elsif ($action eq 'test') {
      $output = $json->encode({
        "error" => "test",
        "headers" => \%headers,
      });
    }
    
    ## The action requires a valid session
    else {
      if (@cookie) {
        my $valid = &validate_session($cookie[0], $cookie[1]);
        $valid = $valid =~ /success/ ? 1 : undef;
        
        if ($valid == 1) {
          if ($action eq 'validate_session') {
            $output = &validate_session($cookie[0], $cookie[1]);
          }
          
          elsif ($action eq 'save_runkeeper_token') {
            if ($headers{"HTTP_RUNKEEPER_TOKEN"}) {
              $output = &save_runkeeper_token();
            } else {
              $output = $json->encode({
                "error" => "0014",
                "description", "runkeeper token is required"
              });
            }
          }
          
          elsif ($action eq 'get_user_home') {
            $output = &get_user_home();
          }
          
          elsif ($action eq 'save_map') {
            if (exists $headers{"HTTP_MAP_NAME"} && $headers{"HTTP_MAP_START_DATE"} && exists $headers{"HTTP_MAP_START_LATITUDE"} && exists $headers{"HTTP_MAP_START_LONGITUDE"} && exists $headers{"HTTP_MAP_END_LATITUDE"} && exists $headers{"HTTP_MAP_END_LONGITUDE"} && exists $headers{"HTTP_MAP_METRIC"} && $headers{"HTTP_MAP_TRAVEL_MODE"} && exists $headers{"HTTP_MAP_ACTIVITIES"}) {
              if (exists $headers{"HTTP_MAP_ID"} && $headers{"HTTP_MAP_ID"} ne 'null') {
                $output = &update_map();
              } else {
                $output = &save_new_map();
              }
            } else {
              $output = $json->encode({
                "error" => "0015",
                "description" => "required headers: map_name, start_lat, start_long, end_lat, end_lon, metric, travel_mode, activities",
                "headers" => \%headers,
              });
            }
          }
          
          elsif ($action eq 'save_settings') {
            if (exists $headers{"HTTP_USERNAME"} || exists $headers{"HTTP_PASSWORD"}) {
              $output = &save_settings();
            } else {
              $output = $json->encode({
                "error" => "0018",
                "description" => "a username or password is required",
              });
            }
          }
          
          elsif ($action eq 'new_osm_key') {
            $output = &new_osm_key();
          }
          
          elsif ($action eq 'get_settings') {
            $output = &get_settings();
          }
          
          elsif ($action eq 'enable_tracking') {
            if (exists $headers{"HTTP_ENABLED"} && ($headers{"HTTP_ENABLED"} eq "0" || $headers{"HTTP_ENABLED"} eq "1")) {
              $output = &enable_tracking();
            } else {
              $output = $json->encode({
                "error" => "0022",
                "description"  => "enabled header is required and must be 0/1"
              });
            }
          }
          
          elsif ($action eq 'log_out') {
            $output = &log_user_out();
          }
          
          ## Update a maps checked date and completion
          elsif ($action eq 'update_map') {
            if (exists $headers{"HTTP_MAP_ID"}) {
            
            } else {
              $output = $json->encode({
                "error" => "0008",
                "description" => "a map ID is required"
              });
            }
          }
          
          # Get RK activities for stats purposes
          elsif ($action eq 'get_stats') {
            if (exists $headers{"HTTP_START_DATE"} && exists $headers{"HTTP_END_DATE"}) {
              $output = &get_stats();
            } else {
              $output = $json->encode({
                "error" => "-",
                "description" => "A start and end date are required"
              });
            }
          }
          
          else {
          $output = $json->encode({
              "error" => "0025",
              "description" => "action header is not recognised: $action",
            });
          }
        } else {
          $output = $json->encode({
            "error" => "0003",
            "description" => "invalid user session",
          });
        }
      } else {
        $output = $json->encode({
          "error" => "0003",
          "description" => "invalid user session",
        });
      }
    }
  } else {
    $output = $json->encode({
      "error" => "0002",
      "description" => "an action header is required",
    });
  }
}


# return JSON string
print $cgi->header(
  -type => "application/json",
  -charset => "utf-8"
);
print $output;
print "\n";









#########################
#  DB Subs
#########################
sub db_connect {
  $dbh = DBI->connect(
    "DBI:mysql:database=runmaps;host=54.243.159.181;port=3306",
    "work_scripts",
    "BVPZbmSywQejvvjB")
    or die $DBI::errstr;
}

sub log_user_in {
  my $sql = "
    SELECT id, session_key, runkeeper_token, elite FROM
      tbl_users
    WHERE
      LOWER(username)=? AND password=?
  ";
  db_connect();
  
  my $sth = $dbh->prepare($sql) or return db_error('log_user_in prepare');
  $sth->execute($headers{"HTTP_USERNAME"}, $headers{"HTTP_PASSWORD"}) or return db_error('log_user_in execute');
  my ($user_id, $session_key, $runkeeper_token, $elite) = $sth->fetchrow_array;
  if ($user_id) {
    return $json->encode({
      "success" => "user details are correct",
      "user_id" => $user_id,
      "username" => $headers{"HTTP_USERNAME"},
      "session_key" => $session_key,
      "runkeeper_token" => $runkeeper_token,
      "elite" => $elite,
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0005",
      "description" => "username or password are incorrect",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub register_user {
  my $key = &generate_random_string(SESSION_KEY_LENGTH);
  my $sql = "
    INSERT INTO tbl_users 
      (username, password, session_key, runkeeper_token, can_login, elite, date_registered, date_last_login)
    VALUES
      (?, ?, ?, ?, ?, ?, ?, ?)
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('register_user prepare');
  $sth->execute($headers{"HTTP_USERNAME"}, $headers{"HTTP_PASSWORD"}, $key, '', 1, 0, time, time) or return db_error('register_user execute');
  my $user_id = $dbh->{mysql_insertid};
  
  $dbh->disconnect;
  return $json->encode({
    "success" => "User " . $headers{"HTTP_USERNAME"} . " has been registered",
    "user_id" => $user_id,
    "session_key" => $key,
    "time_id" => $headers{"HTTP_TIME_ID"},
  });
}

sub verify_username {
  my $sql = "
    SELECT id, username FROM tbl_users WHERE username = ?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('verify_username');
  $sth->execute($headers{"HTTP_USERNAME"}) or return db_error('verify_username');
  my ($id, $username) = $sth->fetchrow_array;
  $dbh->disconnect;
  if ($username) {
    if ("$id" ne "$cookie[0]") {
      return $json->encode({
        "error" => "0007",
        "description" => "username is not unique",
        "test" => "$id - $cookie[0]",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    } else {
      return $json->encode({
        "success" => "current username",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    }
  } else {
    return $json->encode({
      "success" => "username is unique",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub get_map {
  my $sql = "
    SELECT
      id,
      user_id,
      name,
      start_date,
      start_latitude,
      start_longitude,
      end_latitude,
      end_longitude,
      metric,
      travel_mode,
      activities,
      date_created,
      date_viewed
    FROM
      tbl_maps
    WHERE
      id=?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('get_map prepare');
  $sth->execute($headers{"HTTP_MAP_ID"}) or return db_error('get_mapp execute');
  my $results = $sth->fetchrow_hashref;
  $sth->finish;
  $dbh->disconnect;
  if ($results) {
    return $json->encode({
      "success" => "map details: " . $headers{"HTTP_MAP_ID"},
      "details" => $results,
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0011",
      "description" => "no such map id: " . $headers{"HTTP_MAP_ID"},
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub get_stats {
  my $sql = "
    SELECT
      runkeeper_token
    FROM
      tbl_users
    WHERE
      id = ?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('get_stats prepare 1');
  $sth->execute($cookie[0]) or return db_error('get_stats execute 1');
  my ($rk_token) = $sth->fetchrow_array;
  $sth->finish;
  $dbh->disconnect;
  
  my @activities = ();
  
  if (defined $rk_token && $rk_token ne '') {
    my $url_append = "/fitnessActivities?noEarlierThan=" . $headers{"HTTP_START_DATE"} . "&noLaterThan=" . $headers{"HTTP_END_DATE"};
    my $debug = 0;
    
    while (defined $url_append) {
      $debug = $debug + 1;
      my $url = URL . $url_append;
      my $header = HTTP::Headers->new;
      $header->header(
        "Host" => "api.runkeeper.com",
        "Authorization" => "Bearer $rk_token",
        "Accept" => "application/vnd.com.runkeeper.FitnessActivityFeed+json"
      );
      my $request = HTTP::Request->new("GET", $url, $header);
      my $ua = LWP::UserAgent->new;
      my $response = $ua->request($request);
      if ($response->is_success) {
        my $json_obj = new JSON;
        my $json1 = $json_obj->allow_nonref->utf8->relaxed->decode($response->decoded_content);
        $url_append = $json1->{next};
        
        foreach my $activity (@{$json1->{items}}) {
          my $epoch = &convert_rk_date_to_epoch($activity->{start_time});
          my %act = (
            date => $epoch,
            type => $activity->{type},
            distance => $activity->{total_distance},
            duration => $activity->{duration},
            start_time => $activity->{start_time},
            type => $activity->{type},
            uri => $activity->{uri}
          );
          push @activities, \%act;
        }
      } else { # Response is unsuccessful
        undef $url_append;
        my $error = $response->status_line;
        if ($response->status_line =~ /^401/) {
          return $json->encode({
            "error" => "0009",
            "description" => "invalid RunKeeper token",
            "debug" => "$debug"
          });
        } else {
          my $error = $response->status_line;
          return $json->encode({
            "error" => "0010",
            "description" => "error retrieving user history: $error",
          });
        }
      }
    } # END while loop
    
    
    return $json->encode({
      "success" => "Stats retrieved for user $cookie[0]",
      "activities" => \@activities
    });
  } else {
    return $json->encode({
      "error" => "-",
      "description" => "User $cookie[0] has no RunKeeper token"
    });
  }
}

sub get_runkeeper_data {
  ## Get the general map data
  my $sql = "
    SELECT
      tbl_maps.id map_id,
      tbl_maps.user_id,
      tbl_users.runkeeper_token,
      tbl_maps.name,
      tbl_maps.start_date,
      tbl_maps.start_latitude,
      tbl_maps.start_longitude,
      tbl_maps.end_latitude,
      tbl_maps.end_longitude,
      tbl_maps.metric,
      tbl_maps.travel_mode,
      tbl_maps.date_created,
      tbl_maps.activities,
      tbl_maps.date_viewed
    FROM
      tbl_maps
    JOIN
      tbl_users ON tbl_maps.user_id = tbl_users.id
    WHERE
      tbl_maps.id=?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('get_runkeeper_data prepare');
  $sth->execute($headers{"HTTP_MAP_ID"}) or return db_error('get_runkeeper_data execute');
  my $map_details = $sth->fetchrow_hashref;
  $sth->finish;
  
  my @activities = ();
  
  if (defined $map_details) {
    ## Get the users RunKeeper data
    my ($day, $month, $year) = (localtime($map_details->{'start_date'}))[3,4,5];
    $year += 1900;
    $month += 1;
    $month = ($month < 10) ? "0$month" : "$month";
    $day = ($day < 10) ? "0$day" : "$day";
    
    my $url_append = "/fitnessActivities?noEarlierThan=$year-$month-$day";
    my $breaker = 0;
    
    while (defined $url_append) {
      my $url = URL . $url_append;
      my $header = HTTP::Headers->new;
      $header->header(
        "Host" => "api.runkeeper.com",
        "Authorization" => "Bearer $map_details->{runkeeper_token}",
        "Accept" => "application/vnd.com.runkeeper.FitnessActivityFeed+json"
      );
      my $request = HTTP::Request->new("GET", $url, $header);
      my $ua = LWP::UserAgent->new;
      my $response = $ua->request($request);
      if ($response->is_success) {
        my $json_obj = new JSON;
        my $json1 = $json_obj->allow_nonref->utf8->relaxed->decode($response->decoded_content);
        $url_append = $json1->{next};
        
        foreach my $activity (@{$json1->{items}}) {
          my $epoch = &convert_rk_date_to_epoch($activity->{start_time});
          my %act = (
            date => $epoch,
            distance => $activity->{total_distance},
            duration => $activity->{duration},
            type => $activity->{type},
          );
          push @activities, \%act;
        }
      } else {
        undef $url_append;
        my $error = $response->status_line;
        if ($response->status_line =~ /^401/) {
          return $json->encode({
            "error" => "0009",
            "description" => "invalid RunKeeper token",
            "time_id" => $headers{"HTTP_TIME_ID"},
          });
        } else {
          my $error = $response->status_line;
          return $json->encode({
            "error" => "0010",
            "description" => "error retrieving user history: $error",
            "time_id" => $headers{"HTTP_TIME_ID"},
          });
        }
      }
    } # End of while loop
    
    my $total_distance = 0;
    my $total_duration = 0;
    my %month_distance = ();
    my %month_duration = ();
    my %month_activities = ();
    my %month_name = ();
    
    my $month_index = &get_month_index(time());
    # Iterate over each activity and add it to the appropriate month hash
    # an activity on 20-05-2013 would get added to the month_activities{201305} hash
    foreach (@activities) {
      my %activity = %{$_};
      
      if ($map_details->{activities} =~ lc($activity{type})) {
        my $label_date = &get_month_label($activity{'date'});
        my $index_date = &get_month_index($activity{'date'});
        $month_distance{$index_date} += $activity{'distance'};
        $month_duration{$index_date} += $activity{'duration'};
        $month_name{$index_date} = $label_date;
        
        $month_activities{$index_date}{$activity{'date'}} = {
          'distance'=>$activity{'distance'},
          'duration'=>$activity{'duration'},
          'type'=>$activity{'type'}
        };
        
        $total_distance += $activity{'distance'};
        $total_duration += $activity{'duration'};
      }
    }
    
    my %months = ();
    foreach my $key (keys %month_distance) {
      $months{$key} = {
        'name'=>$month_name{$key},
        'distance'=>$month_distance{$key},
        'duration'=>$month_duration{$key},
        'activities'=>$month_activities{$key}
      };
    }
    
    $sql = "
      UPDATE
        tbl_maps
      SET
        date_viewed=?
      WHERE
        id=?
    ";
    $dbh->do($sql, undef, time, $map_details->{'map_id'});
    
    $sth->finish;
    $dbh->disconnect;
    
    return $json->encode({
      "success" => "data for map " . $map_details->{'map_id'},
      "total_distance"=>"$total_distance",
      "total_duration"=>"$total_duration",
      "months"=>\%months,
      "action" => "$action",
      "headers" => \%headers,
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0011",
      "description" => "no such map id: " . $headers{"HTTP_MAP_ID"},
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub get_location {
  my $sql = "
    SELECT
      user_id, latitude, longitude, time, hdop, altitude, speed
    FROM
      tbl_position
    WHERE
      user_id=?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('get_location prepare');
  $sth->execute($headers{"HTTP_USER_ID"})  or return db_error('get_location execute');
  my $result = $sth->fetchrow_hashref;
  $sth->finish;
  $dbh->disconnect;
  if ($result) {
    return $json->encode({
      "success" => "user location data",
      "details" => $result,
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0013",
      "description" => "no location data for user",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub save_runkeeper_token {
  my $sql = "
    UPDATE
      tbl_users
    SET
      runkeeper_token=?
    WHERE
      id = ?
  ";
  db_connect();
  my $result = $dbh->do($sql, undef, $headers{"HTTP_RUNKEEPER_TOKEN"}, $cookie[0]) or return db_error('save_runkeeper_token do');
  if ($result == 1) {
    return $json->encode({
      "success" => "User token updated",
      "user_id" => $cookie[0],
      "runkeeper_token" => $headers{"HTTP_RUNKEEPER_TOKEN"},
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "",
      "description" => "Too many/few updates",
      "user_id" => $cookie[0],
      "results" => "$result",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub get_user_home {
  my $sql = "
    SELECT
      \@row := \@row + 1 row,
      maps.id, maps.name, maps.start_date, maps.start_latitude,
      maps.start_longitude, maps.end_latitude, maps.end_longitude,
      maps.metric, maps.travel_mode, maps.date_created, maps.date_viewed
    FROM
      tbl_maps maps, (SELECT \@row := -1) row
    WHERE
      maps.date_deleted IS NULL AND maps.user_id = ?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('get_user_home_details');
  $sth->execute($cookie[0]) or return db_error('get_user_home_details');
  my $maps_ref = $sth->fetchall_hashref('row');
  my $map_count = keys(%{$maps_ref});
  my @maps = @{$maps_ref}{0 .. $map_count};
  @maps = grep { !/^$/ } @maps;

  return $json->encode({
    "success" => "users home details",
    "map_count" => "$map_count",
    "maps" => \@maps,
    "time_id" => $headers{"HTTP_TIME_ID"},
  });
}

sub update_map {
  my $sql = "
    UPDATE
      tbl_maps
    SET
      name=?,
      start_date=?,
      start_latitude=?,
      start_longitude=?,
      end_latitude=?,
      end_longitude=?,
      metric=?,
      travel_mode=?,
      activities=?
    WHERE
      id=?
  ";
  db_connect();
  my $result = $dbh->do($sql, undef, (
    $headers{"HTTP_MAP_NAME"},
    $headers{"HTTP_MAP_START_DATE"},
    $headers{"HTTP_MAP_START_LATITUDE"},
    $headers{"HTTP_MAP_START_LONGITUDE"},
    $headers{"HTTP_MAP_END_LATITUDE"},
    $headers{"HTTP_MAP_END_LONGITUDE"},
    $headers{"HTTP_MAP_METRIC"},
    $headers{"HTTP_MAP_TRAVEL_MODE"},
    $headers{"HTTP_MAP_ACTIVITIES"},
    $headers{"HTTP_MAP_ID"}
  )) or return db_error ('update_map do');
  if ($result == 1) {
    return $json->encode({
      "success" => "map " . $headers{"HTTP_MAP_ID"} . "  has been saved",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } elsif ($result > 1) {
    return $json->encode({
      "error" => "0016",
      "description" => "too many records updated",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0017",
      "description" => "map not updated",
      "details" => exists $headers{"HTTP_MAP_ID"},
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub save_new_map {
  my $sql = "
    INSERT INTO tbl_maps
    (
      user_id, name, start_date,
      start_latitude, start_longitude,
      end_latitude, end_longitude,
      metric, travel_mode, activities,
      date_created
    )
    VALUES
    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
  ";
  my $sth = $dbh->prepare($sql) or return db_error('save_new_map prepare');
  $sth->execute(
    $cookie[0], $headers{"HTTP_MAP_NAME"}, $headers{"HTTP_MAP_START_DATE"},
    $headers{"HTTP_MAP_START_LATITUDE"}, $headers{"HTTP_MAP_START_LONGITUDE"},
    $headers{"HTTP_MAP_END_LATITUDE"}, $headers{"HTTP_MAP_END_LONGITUDE"},
    $headers{"HTTP_MAP_METRIC"}, $headers{"HTTP_MAP_TRAVEL_MODE"}, $headers{"HTTP_MAP_ACTIVITIES"},
    time
  ) or return db_error('save_new_map execute');
  my $map_id = $dbh->{mysql_insertid};
  if ($map_id) {
    return $json->encode({
      "success" => "map saved",
      "map_id" => $map_id,
      "details" => $headers{"HTTP_MAP_START_DATE"},
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0017",
      "description" => "map not saved",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}

sub get_settings {
  my $sql = "
    SELECT
      tbl_users.id user_id,
      tbl_users.username,
      tbl_position.enabled position_enabled,
      tbl_position.osm_key
    FROM
      tbl_users
    LEFT JOIN
      tbl_position ON tbl_position.user_id = tbl_users.id
    WHERE
      tbl_users.id=?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('get_settings prepare');
  $sth->execute($cookie[0]) or return db_error('get_settings execute');
  my $results = $sth->fetchrow_hashref;
  $sth->finish;
  $dbh->disconnect;
  
  return $json->encode({
    "success" => "settings for user",
    "details" => $results,
    "time_id" => $headers{"HTTP_TIME_ID"},
  })
}

sub enable_tracking {
  my $sql = "
    UPDATE tbl_position SET enabled=? WHERE user_id=?
  ";
  db_connect();
  my $result = $dbh->do($sql, undef, $headers{"HTTP_ENABLED"}, $cookie[0]) or return db_error ('enable_tracking do');
  if ($result == 1) {
    return $json->encode({
      "success" => "tracking enabled/disabled (" . $headers{"HTTP_ENABLED"} . ")",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } elsif ($result > 1) {
    return $json->encode({
      "error" => "0016",
      "description" => "too many records updated",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0023",
      "description" => "tracking not saved",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}




























sub save_current_location {
  my $sql = "
    UPDATE
      tbl_position
    SET
      latitude = ?,
      longitude = ?,
      time = ?,
      hdop = ?,
      altitude = ?,
      speed = ?
    WHERE
      osm_key = ?
  ";
  my @params;
  if (
    defined $cgi->param('lat')
    && defined $cgi->param('lon')
    && defined $cgi->param('time')
    && defined $cgi->param('hdop')
    && defined $cgi->param('alt') && defined $cgi->param('speed') && defined $cgi->param('osm')) {
    push @params, $cgi->param('lat');
    push @params, $cgi->param('lon');
    push @params, $cgi->param('time');
    push @params, $cgi->param('hdop');
    push @params, $cgi->param('alt');
    push @params, $cgi->param('speed');
    push @params, $cgi->param('osm');
    db_connect();
    
    my $result = $dbh->do ($sql, undef, @params) or return db_error ('save_current_location do');
    if ($result == 1) {
      return $json->encode({
        "success" => "location updated",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    } elsif ($result > 1) {
      return $json->encode({
        "error" => "",
        "description" => "too many records updated",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    } else {
      return $json->encode({
        "error" => "",
        "description" => "tracking not saved",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    }
  } else {
    $sql = "
      UPDATE
        tbl_position
      SET
        error = 'Your OSM URL is incorrect, so your location can't be updated. Please check the app settings'
      WHERE
        osm_key = ?
    ";
  }
}


sub log_user_out {
  my $key;
  my $sql = "
    UPDATE
      tbl_users
    SET
      session_key = ?
    WHERE
      id = ?
  ";
  db_connect();
  my $breaker = 0;
  while ($breaker < 100) {
    $key = &generate_random_string(SESSION_KEY_LENGTH);
    my $result = $dbh->do($sql, undef, $key, $cookie[0]) or return db_error ('log_user_out do');
    if ($result == 1) {
      return $json->encode({
        "success" => "session key changed",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    } elsif ($result > 1) {
      return $json->encode({
        "error" => "0016",
        "description" => "too many records updated: $cookie[0]",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    } else {
      return $json->encode({
        "error" => "0024",
        "description" => "session key not changed",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    }
  }
}

sub new_osm_key {
  my $key;
  my $sql = "
    SELECT id FROM tbl_position WHERE osm_key=?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('new_osm_key prepare');
  my $breaker = 0;
  while ($breaker < 100) {
    $key = &generate_random_string(OSM_KEY_LENGTH);
    $sth->execute($key) or return db_error('new_osm_key execute');
    my ($result) = $sth->fetchrow_array;
    $sth->finish;
    if ($result) {
      $breaker++;
    } else {
      $breaker = 1000;
    }
  }
  if ($breaker == 100) {
    return $json->encode({
      "error" => "0020",
      "description" => "loop caught",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    $sql = "
      INSERT INTO
        tbl_position (user_id, osm_key)
      VALUES
        (?, ?)
      ON DUPLICATE KEY UPDATE
        osm_key = ?
    ";
    #$sth = $dbh->prepare($sql) or return db_error('new_osm_key prepare');
    
    my $result = $dbh->do($sql, undef, $cookie[0], $key, $key) or return db_error('new_osm_key do');
    if ($result >= 1) {
      return $json->encode({
        "success" => "osm key",
        "details" => "$key",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    } else {
      return $json->encode({
        "error" => "0021",
        "description" => "osm key not saved",
        "time_id" => $headers{"HTTP_TIME_ID"},
        "RESULT" => "$result",
        "key" => "$key",
        "user" => $cookie[0],
      });
    }
  }
}

sub save_settings {
  my @params;
  my $sql = "
    UPDATE
      tbl_users
    SET
  ";
  if (exists $headers{"HTTP_USERNAME"}) {
    $sql .= " username=?";
    push @params, $headers{"HTTP_USERNAME"};
  }
  if (exists $headers{"HTTP_PASSWORD"}) {
    $sql .= $sql =~ /username/ ? ", password=?" : " password=?";
    push @params, $headers{"HTTP_PASSWORD"};
  }
  $sql .= " WHERE id=?";
  push @params, $cookie[0];
  
  db_connect();
  my $result = $dbh->do($sql, undef, @params) or return db_error('save_settings do');
  if ($result == 1) {
    return $json->encode({
      "success" => "user settings saved",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } elsif ($result < 1) {
    return $json->encode({
      "error" => "0016",
      "description" => "too many records updated: $cookie[0]",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  } else {
    return $json->encode({
      "error" => "0019",
      "description" => "user settings not saved $cookie[0]",
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}




sub validate_session {
  my ($user_id, $session_key) = @_;
  my $sql = "
    SELECT id, runkeeper_token FROM tbl_users
    WHERE id=? AND session_key=?
  ";
  db_connect();
  my $sth = $dbh->prepare($sql) or return db_error('is_session_valid prepare');
  $sth->execute($user_id, $session_key) or return db_error('is_session_valid execute');
  my ($result, $token) = $sth->fetchrow_array;
  $sth->finish;
  $dbh->disconnect;
  if ($result) {
    if ($token) {
      return $json->encode({
        "success" => "user session is valid",
        "user_id" => "$user_id",
        "session_key" => "$session_key",
        "runkeeper_token" => $token,
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    } else {
      return $json->encode({
        "success" => "user session is valid",
        "user_id" => "$user_id",
        "session_key" => "$session_key",
        "token" => "none",
        "time_id" => $headers{"HTTP_TIME_ID"},
      });
    }
  } else {
    return $json->encode({
      "error" => "0003",
      "description" => "invalid user session",
      "user_id" => "$user_id",
      "session_key" => "$session_key",
      "headers" => \%headers,
      "time_id" => $headers{"HTTP_TIME_ID"},
    });
  }
}











########################
# Other subs
########################
sub generate_random_string {
  my ($length) = @_;
  return undef unless $length;
  
  my @chars = ('a'..'z', 'A'..'Z', '0'..'9');
  my $key = '';
  
  foreach (1..$length) {
    $key .= $chars[rand @chars];
  }
  return $key;
}
sub db_error {
  my ($sub_name) = @_;
  my $sql = $dbh->{Statement};
  $sql =~ s/\n//g;
  $sql =~ s/\s+/ /g;
  return $json->encode({
    "error" => "0000",
    "description" => "DB error (" . $sub_name . "): " . $dbh->errstr,
#    "sql" => $dbh->trace($dbh->parse_trace_flags('SQL|1|test')),
    "sql" => $sql,
  });
}

sub convert_rk_date_to_epoch {
  my %months = (
    "Jan" => 1,
    "Feb" => 2,
    "Mar" => 3,
    "Apr" => 4,
    "May" => 5,
    "Jun" => 6,
    "Jul" => 7,
    "Aug" => 8,
    "Sep" => 9,
    "Oct" => 10,
    "Nov" => 11,
    "Dec" => 12
  );
  my ($str_date) = @_;
  if (defined $str_date) {
    my ($dow, $day, $str_month, $year, $time) = split(' ', $str_date);
    my ($hour, $min, $sec) = split(':', $time);
    my $month = $months{$str_month};
    my $datetime = DateTime->new(
      year => $year,
      month => $month,
      day => $day,
      hour => $hour,
      minute => $min,
      second => $sec
    );
    return $datetime->epoch();
  } else {
    return undef;
  }
}
sub get_month_index {
  my ($epoch) = @_;
  my $date = DateTime->from_epoch(epoch=>$epoch);
  my $mon = ($date->month() < 10) ? "0" . $date->month : $date->month;
  my $year = $date->year();
  return "$year$mon";
}
sub get_month_label {
  my ($epoch) = @_;
  my $date = DateTime->from_epoch(epoch=>$epoch);
  return $date->month_abbr() . ' ' . $date->year();
}
