#!/usr/bin/perl

use strict;
use warnings;
use CGI qw(:standard);
use HTTP::Request;
use LWP::UserAgent;
use Cwd;

my $cgi = CGI->new;

my $code = $cgi->param('code');
my $base_url = "../authorise.html";

if ($code) {
## This code is only used for debugging
#&print_html("Has a code: $code");

  my $client_id = "7d87ecffed274bd280f08ad811814795";
  my $client_secret = "2111e67910274eab921b39ba75f15f65";
  my $redirect_uri = "http://" . $ENV{'HTTP_HOST'} . $ENV{'SCRIPT_NAME'};
  
  my $url = "https://runkeeper.com/apps/token";
  $url .= "?grant_type=authorization_code";
  $url .= "&code=$code";
  $url .= "&client_id=$client_id";
  $url .= "&client_secret=$client_secret";
  $url .= "&redirect_uri=$redirect_uri";
  
  my $request = HTTP::Request->new(POST => $url);
  $request->content_type('application/x-www-form-urlencoded');
  my $ua = LWP::UserAgent->new;
  my $resp = $ua->request($request);
  
  if ($resp->is_success) {
    my $text = $resp->decoded_content;
    my ($token) = $text =~ /"(\w+)"}/;
#&print_html($token);
    
    print redirect(-url=>$base_url . "?code=$token");
  } else {
#&print_html('Response failed');
    print redirect(-url=>$base_url . "?resp=" . "failed");
  }
} else {
  print redirect(-url=>$base_url . "?resp=else");
}

sub print_html {
  my ($msg) = @_;
  print $cgi->header;
  print $cgi->start_html;
  print $cgi->h2($msg);
  print $cgi->end_html;
}
