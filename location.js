var debug = 1;
var output_map;
var position_marker;
var position;

$(document).ready(function() {
  try {
    if (validate_session()) {
      $('#div_navbar').html(html_navbar);
    }
    google.maps.event.addDomListener(window, 'load', initialise());
  } catch (e) {
    log ('location ready error: ' + e);
  }
});

function initialise() {
  try {
    $('#progress_status').show();
    $('#progress_bar').progressBar(20);
    var properties = {
      center: new google.maps.LatLng(0,0),
      zoom: 2,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    output_map = new google.maps.Map(document.getElementById("output_map"), properties);
    
    google.maps.event.addListenerOnce(output_map, 'idle', function() {
      get_location();
    });
  } catch (e) {
    log ('initialise error: ' + e);
  }
}

function get_location() {
  try {
    $('#progress_bar').progressBar(40);
    $.ajax({
      type: "GET",
      url: config.url_api,
      beforeSend: function(xhr) {
        xhr.setRequestHeader ('action', 'get_location');
        xhr.setRequestHeader ('user_id', $.url_param('id'));
      },
      error: function (req, text, status) {
        log ('get_location http error: ' + e);
      },
      success: function (resp) {
        try {
          if (resp.error) {
            if (/0017/.test(resp.error)) {
              alert ('There is no ID to get the location for');
            } else {
              log ('get_location response error: ' + resp.error);
            }
          } else {
            if (resp.details.latitude == null) {
              $('#output_stats').html("<h5>No location data available</h5>");
            } else {
              $('#progress_bar').progressBar(60);
              position = resp.details;
              position_marker = add_marker({
                position: new google.maps.LatLng(position.latitude, position.longitude),
                title: 'Location',
                icon: icon_location,
                map: output_map
              }, false, '');
              
              output_map.setZoom(14);
              output_map.panTo(position_marker.getPosition());
              
              var circle_options = {
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: output_map,
                center: position_marker.getPosition(),
                radius: 10 * (resp.details.hdop / 2) * resp.details.hdop
              };
              
              var circle = new google.maps.Circle(circle_options);
              
              var ptext = '<table class="table">';
              ptext += '<tr><td>Location</td><td>';
              
              var stext = '</td></tr>';
              stext += '<tr><td>Time</td><td>' + (new Date(parseInt(position.time)).strDate('dd-mon-yyyy HH:MM:SS tmz')) + '</td></tr>';
              stext += '<tr><td>Speed</td><td>' + position.speed  + '</td></tr>';
              stext += '<tr><td>Altitude</td><td>' + position.altitude  + '</td></tr>';
              stext += '<tr><td>HDOP</td><td>' + position.hdop  + '</td></tr>';
              stext += '</table>';
              
              get_place_name(position_marker.getPosition(), 'output_stats', ptext, stext);
            }
          }
        } catch (e) {
          log ('get_location success error: ' + e);
        }
      }
    });
  } catch (e) {
    log ('get_location error: ' + e);
  }
}

function get_place_name(pos, id, prefix, suffix) {
  try {
    $('#progress_bar').progressBar(80);
    if (pos) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({'latLng': pos}, function (results, status) {
        try {
          if (status == google.maps.GeocoderStatus.OK) {
            $('#progress_bar').progressBar(100);
            if (results[1]) {
              var addr = results[1].formatted_address;
              var addr1 = addr.substr(0, addr.indexOf(','));
              var addr2 = addr.substr(addr.lastIndexOf(','));
              $('#' + id).html(prefix + addr1 + addr2 + suffix);
            }
          } else {
            if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
              log ('get_place_name http error: ' + status);
            }
          }
          $('#progress_status').delay(2000).fadeOut();
        } catch (e) {
          log ('get_place_name request error: ' + e);
        }
      });
    } else {
      $('#' + id).html(prefix + 'No position given' + suffix);
    }
  } catch (e) {
    log ('get_place_name error: ' + e);
  }
}
