///     Get an element from an associative array by key/value
Object.getElementByValue = function (key, value) {
  var element;
  $.each(this, function(index, value) {
    if (this[key] == value) {
      element = this;
      return false;
    }
  });
  return element;
}

///     Search for an object in an array by value
$.search_by_value = function searchByValue(obj, name, value) {
  for (var key in obj) {
    if (obj[key][name] == value) {
      return obj[key];
    }
  }
}

///     Calculate the mid point between two co ordinates
$.mid_point = function(lat1, lon1, lat2, lon2) {
  $.console ('DEPRECATED mid_point. Use $.get_mid_point instead');
  try {
    lat1 = (lat1-0).toRadians();
    lon1 = (lon1-0).toRadians();
    lat2 = (lat2-0).toRadians();
    lon2 = (lon2-0).toRadians();
    
    var bx = Math.cos(lat2) * Math.cos(lon2 - lon1);
    var by = Math.cos(lat2) * Math.sin(lon2 - lon1);
    var lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + bx) * (Math.cos(lat1) + bx) + by * by));
    var lon3 = lon1 + Math.atan2(by, Math.cos(lat1) + bx);
    
    return [lat3.toDegrees(), lon3.toDegrees()];
  } catch (e) {
log ('mid_point');
    throw e;
  }
}

///     Calcualte approx distance between two co ordinates
$.distance_between = function(lat1, lon1, lat2, lon2) {
  try {
    var r = 6371;
    var o1 = (lat1-0).toRadians();
    var o2 = (lat2-0).toRadians();
    var to = (lat2-lat1).toRadians();
    var ty = (lon2-lon1).toRadians();
    
    var a = Math.sin(to/2) * Math.sin(to/2) +
      Math.cos(o1) * Math.cos(o2) *
      Math.sin(ty/2) * Math.sin(ty/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return r * c;
  } catch (e) {
log ('distance_between');
    throw e;
  }
}

///     Convert a negree to radians
Number.prototype.toRadians = function() {
  try {
    return this * Math.PI / 180;
  } catch (e) {
log ('toRadians');
    throw e;
  }
}
///     Convert a radian to degrees
Number.prototype.toDegrees = function() {
  try {
    return this * 180 / Math.PI;
  } catch (e) {
log ('toDegrees');
    throw e;
  }
}

///     Converts seconds to hh:mm:ss
Number.prototype.toHours = function() {
  var seconds = parseFloat(this);
  var hours = parseInt(seconds / 3600);
  var minutes = parseInt(seconds / 60) % 60;
  seconds = (seconds % 60).toFixed(0);
  return (hours < 10 ? '0'+hours : hours) + ':' + (minutes < 10 ? '0'+minutes : minutes) + ':' + (seconds < 10 ? '0'+seconds : seconds);
}

Object.size = function(obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
}

/// Sort an associative array by a value
function value_sort(property) {
  try {
log (property);
    var sort_order = 1;
    if (property[0] === '-') {
      sort_order = -1;
      property = property.substr(1);
    }
    return function(a,b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1: 0;
      return result * sort_order;
    }
  } catch (e) {
    throw e;
  }
}

///      Converts distances
Number.prototype.convertDistance = function (input, output, decimals) {
  try {
  var metres;
  input = $.trim(input);
  output = $.trim(output);
  if (!decimals && decimals != 0) {
    decimals = 2;
  }
  switch (input) {
    case 'mm':
    case 'millimetres':
      metres = this / 1000;
      break;
    case 'cm':
    case 'centimetres':
      metres = this / 100;
      break;
    case 'dm':
    case 'decimetres':
      metres = this / 10;
      break;
    case 'm':
    case 'metres':
      metres = this;
      break;
    case 'km':
    case 'kilometres':
      metres = this * 1000;
      break;
    case 'ft':
    case 'feet':
      metres = this * 0.3048;
      break;
    case 'yd':
    case 'yards':
      metres = this * 0.9144;
      break;
    case 'mi':
    case 'miles':
      metres = this * 1609.344;
      break;
    case 'nm':
    case 'nautical_miles':
      metres = this * 1852;
      break;
    default:
      throw "InvalidInput: " + input;
      break;
  }
  switch (output) {
    case 'mm':
    case 'millimetres':
      metres = metres * 1000;
      break;
    case 'cm':
    case 'centimetres':
      metres = metres * 100;
      break;
    case 'dm':
    case 'decimetres':
      metres = metres * 10;
      break;
    case 'm':
    case 'metres':
      metres = metres;
      break;
    case 'km':
    case 'kilometres':
      metres = metres * 0.001;
      break;
    case 'ft':
    case 'feet':
      metres = metres * 3.2808399;
      break;
    case 'yd':
    case 'yards':
      metres = metres * 1.0936133;
      break;
    case 'mi':
    case 'miles':
      metres = metres * 0.000621371192;
      break;
    case 'nm':
    case 'nautical_miles':
      metres = metres * 0.000539956803;
      break;
    default:
      throw "InvalidOutput: " + output;
      break;
    }
    return metres.toFixed(decimals);
  } catch (e) {
    throw e;
  }
};

///     Change the progress of a progress percentage .progress-bar
$.fn.progressBar = function(progress, html) {
  this.attr('style', 'width: ' + progress + '%;');
  if (html) {
    this.html = html;
  }
  return this;
};

///     Retrieve URL parameters by name
$.url_param = function(name) {
  try {
    return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
  } catch (e) {
    return null;
  }
}

///     HTML Builder from: http://marcgrabanski.com/building-html-in-jquery-and-javascript/
$.build = function(tag, attrs, html) {
  // you can skip html param
  if (typeof(attrs) == 'string') {
    html = attrs;
    attrs = null;
  }
  var h = '<' + tag;
  for (attr in attrs) {
    if(attrs[attr] === false) continue;
    h += ' ' + attr + '="' + attrs[attr] + '"';
  }
  return h += html ? ">" + html + "</" + tag + ">" : "></" + tag + ">";
}

///     Simple string formatter for a Date
Date.prototype.strDate = function(format) {
  try {
    if (/mon/.test(format)) {
      var months = { 0: 'Jan', 1: 'Feb', 2: 'Mar', 3: 'Apr', 4: 'May', 5: 'Jun', 6: 'Jul', 7: 'Aug', 8: 'Sep', 9: 'Oct', 10: 'Nov', 11: 'Dec' };
      format = format.replace('mon', months[this.getMonth()]);
    }
    if (/dow/.test(format)) {
      var days = { 0: 'Sun', 1: 'Mon', 2: 'Tue', 3: 'Wed', 4: 'Thu', 5: 'Fri', 6: 'Sat' };
      format = format.replace('dow', days[this.getDay()]);
    }
    if (/doy/.test(format)) {
      var onejan = new Date(this.getFullYear(),0,1);
      format = format.replace('doy', Math.ceil((this - onejan) / 86400000) + 1);
    }
    if (/tmz/.test(format)) {
      format = format.replace('tmz', 'GMT+' + ((this.getTimezoneOffset()/60 > 0) ? 0 - this.getTimezoneOffset()/60 : Math.abs(this.getTimezoneOffset()/60)));
    }
    
    if (/dd/.test(format)) {
      format = format.replace('dd', ((this.getDate() < 10) ? '0' + this.getDate() : '' + this.getDate()));
    } else if (/d\W/.test(format)) {
      format = format.replace('d', ('' + this.getDate()));
    }
    if (/mm/.test(format)) {
      format = format.replace('mm', (((this.getMonth()+1) < 10) ? '0' + (this.getMonth()+1) : '' + (this.getMonth()+1)));
    } else if (/m\W/.test(format)) {
      format = format.replace('m', (this.getMonth()+1));
    }
    if (/yyyy/.test(format)) {
      format = format.replace('yyyy', (this.getFullYear()));
    } else if (/yy/.test(format)) {
      format = format.replace('yy', ('' + this.getFullYear()).substr(2,2));
    }
    
    if (/HH/.test(format)) {
      format = format.replace('HH', ((this.getHours() < 10) ? '0' + this.getHours() : '' + this.getHours()));
    } else if (/H/.test(format)) {
      format = format.replace('H', ('' + this.getHours()));
    }
    if (/MM/.test(format)) {
      format = format.replace('MM', ((this.getMinutes() < 10) ? '0' + this.getMinutes() : '' + this.getMinutes()));
    } else if (/M\W/.test(format)) {
      format = format.replace('M', ('' + this.getMinutes()));
    }
    if (/SS/.test(format)) {
      format = format.replace('SS', ((this.getSeconds() < 10) ? '0' + this.getSeconds() : '' + this.getSeconds()));
    } else if (/S\W/.test(format)) {
      format = format.replace('S', ('' + this.getSeconds()));
    }
  } catch (e) {
    return null;
  }
  return format;
};























function sort_array(arr) {
  var sorted_keys = new Array();
  var sorted_obj = {};
  
  for (var x in arr) {
    sorted_keys.push(x);
  }
  sorted_keys.sort();
  
  for (var x in sorted_keys) {
    sorted_obj[sorted_keys[x]] = arr[sorted_keys[x]];
  }
  return sorted_obj;
}














var log_counter = 0;
function log (msg, append, time) {
  if (time) {
    $('#div_error').show().delay(time).fadeOut();
  } else {
    $('#div_error').show();
  }
//  msg = new Date().getTime() + ": " + msg + '<br>';
  msg = log_counter + ': ' + msg + '<br>';
  log_counter++;
  if (append) {
    $('#div_error').html(msg);
  } else {
    $('#div_error').append(msg);
  }
}
function log_user_error (msg, time) {
  if (time) {
    $('#div_user_error').show().delay(time).fadeOut();
  } else {
    $('#div_user_error').show();
  }
  $('#div_user_error').html(msg);
}
function hide_user_error() {
  $('#div_user_error').hide();
}

function log_success (msg, time) {
  if (time) {
    $('#div_success').show().delay(time).fadeOut();
  } else {
    $('#div_success').show();
  }
  $('#div_success').html(msg);
}
function show_alert(id, msg, time) {
  if (time) {
    $('#' + id).show().delay(time).fadeOut();
  } else {
    $('#' + id).show();
  }
  $('#' + id).html(msg);
}
