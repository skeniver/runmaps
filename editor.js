var debug = 1;
var output_map;
var directions_service;
var directions_output;
var right_click_location;
var output_context;
var start_position;
var start_marker;
var end_position;
var end_marker;

var map_id;
var map;

$(document).ready(function () {
  try {
    if (validate_session()) {
      $('#div_navbar').html(html_navbar);
      $('#input_name').focus();
      
      map_id = $.url_param('map_id');
      google.maps.event.addDomListener(window, 'load', initialise());
    } else {
      window.location.href = "login.html";
    }
  } catch (e) {
    log ('ready error: ' + e);
  }
});

function load_map_details() {
  try {
    $.ajax({
      type: "GET",
      url: config.url_api,
      async: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('action', 'get_map');
        xhr.setRequestHeader('map_id', map_id);
      },
      error: function (req, text, status) {
        log ('load_map_details http error: ' + e);
      },
      success: function (resp) {
        try {
          if (resp.error) {
            log ('load_map_details response error: ' + resp.error);
          } else {
            map = resp.details;
            
            $('#input_name').val(map.name);
            $('#input_start_lat').val(map.start_latitude);
            $('#input_start_lon').val(map.start_longitude);
            $('#input_end_lat').val(map.end_latitude);
            $('#input_end_lon').val(map.end_longitude);
            $('#input_travel_mode').val(map.travel_mode);
            $('#input_date').val(new Date(map.start_date*1000).toISOString().split('T')[0]);
            $('input:radio[name=check_metric]').filter('[value=' + map.metric  + ']').prop('checked', true);
            $('#input_activities').val(map.activities.split(';'));
            
            start_position = new google.maps.LatLng(map.start_latitude, map.start_longitude);
            end_position = new google.maps.LatLng(map.end_latitude, map.end_longitude);
            start_marker = add_marker({
              position: start_position,
              title: "Start",
              map: output_map,
              icon: icon_start,
              draggable: false
            }, false, '');
            end_marker = add_marker({
              position: end_position,
              title: "Finish",
              map: output_map,
              icon: icon_end,
              draggable: false
            }, false, '');
            get_directions(start_position, end_position);
          }
        } catch (e) {
          log ('load_map_details success error: ' + e);
        }
      }
    });
  } catch (e) {
    log ('load_map_details error: ' + e);
  }
}

function save_map() {
  try {
    if ($('#input_name').val()) {
      $.ajax({
        type: "GET",
        url: config.url_api,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('action', 'save_map');
          if (map_id) {
            xhr.setRequestHeader('map_id', map_id);
          }
          xhr.setRequestHeader('map_name', $('#input_name').val());
          xhr.setRequestHeader('map_start_date', new Date($('#input_date').val()).getTime()/1000);
          xhr.setRequestHeader('map_start_latitude', $('#input_start_lat').val());
          xhr.setRequestHeader('map_start_longitude', $('#input_start_lon').val());
          xhr.setRequestHeader('map_end_latitude', $('#input_end_lat').val());
          xhr.setRequestHeader('map_end_longitude', $('#input_end_lon').val());
          xhr.setRequestHeader('map_metric', $('#input_metric').val());
          xhr.setRequestHeader('map_travel_mode', $('#input_travel_mode').val());
          xhr.setRequestHeader('map_activities', $('#input_activities').val().join(';'));
        },
        error: function (req, text, status) {
          log ('save_map http error: ' + text);
        },
        success: function (resp) {
          if (resp.error) {
            log ('save_map resp error: ' + resp.error);
            log ('details: ' + resp.details);
          } else {
            alert ('map saved!');
            window.location.href = 'home.html';
          }
        }
      });
    } else {
      log_user_error ('You must enter a name for your map');
      $('#input_map_name').focus();
    }
  } catch (e) {
    log ('save_map error: ' + e);
  }
}

function initialise() {
  try {
    // Set the map and it's defaults
    var properties = {
      center: new google.maps.LatLng(0,0),
      zoom: 2,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    output_map = new google.maps.Map(document.getElementById("output_map"), properties);
    
    // Setup the directions service
    directions_service = new google.maps.DirectionsService();
    directions_output = new google.maps.DirectionsRenderer();
    directions_output.setMap(output_map);
    
    // Set the map event listeners
    google.maps.event.addListener(output_map, "rightclick", function(event) {
      // Show the context menu when a right click is detected
      try {
        right_click_location = event.latLng;
        show_context_menu(event);
      } catch (e) {
        log ('initialise rightclick error: ' + e);
      }
    });
    google.maps.event.addListener(output_map, "click", function(event) {
      try {
        if (output_context) {
          output_context.style.visiblity = "hidden";
        }
      } catch (e) {
        log ('initialise click error: ' + e);
      }
    });
    
    google.maps.event.addListenerOnce(output_map, 'idle', function() {
      if (map_id != 'null') {
        /// User is editing an exisitng map
        load_map_details(); 
      }
    });
  } catch (e) {
    log ('initialise error: ' + e);
  }
}

function search_map() {
  try {
    geocoder = new google.maps.Geocoder();
    
    hide_user_error();
    if ($('#input_search').val()) {
      $('#input_search').prop('disabled', 'disabled');
      geocoder.geocode({ address: $('#input_search').val() }, function (results, status) {
        try {
          $('#input_search').removeAttr('disabled');
          if (status == google.maps.GeocoderStatus.OK) {
            output_map.setCenter(results[0].geometry.location);
            output_map.setZoom(13);
          } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
            user_error ('There are no search results');
          } else {
            log ('search_map status error: ' + e);
          }
        } catch (e) {
          log ('search_map result error: ' + e);
        }
      });
    }
  } catch (e) {
    log ('search_map error: ' + e);
  }
}

function show_context_menu(event) {
  try {
    var projection = output_map.getProjection();
    $('.context_menu').remove();
    
    output_context = document.createElement('div');
    output_context.className = "context_menu";
    
    var context_content = '<a class="menu_links"><div class=context onclick="make_location(1);">Start here</div></a>';
    context_content += '<a class="menu_links"><div class=context onclick="make_location();">End here</div></a>';
    output_context.innerHTML = context_content;
    
    $(output_map.getDiv()).append(output_context);
    
    var map_width = $('#output_map').width();
    var map_height = $('#output_map').height();
    var menu_width = $('.context_menu').width();
    var menu_height = $('.context_menu').height();
    var clicked_position = getCanvasXY(event.latLng);
    var menu_x = clicked_position.x;
    var menu_y = clicked_position.y;
    
    if ((map_width - menu_x) < menu_width) {
      menu_x = menu_x - menu_width;
    }
    if ((map_height - menu_y) < menu_height) {
      menu_y = menu_y - menu_height;
    }
    $('.context_menu').css('left', menu_x);
    $('.context_menu').css('top', menu_y);
    
    output_context.style.visibility = "visible";
  } catch (e) {
    log ('show_context_menu error: ' + e);
  }
}

function change_travel_mode() {
  if (start_marker && end_marker) {
    get_directions(start_marker.getPosition(), end_marker.getPosition());
  }
}

function make_location(start) {
  try {
    var icon;
    var title;
    var input = '#input_';
    
    if (start) {
      if (start_marker) {
        start_marker.setMap(null);
      }
      icon = icon_start;
      title = "Start";
      input += 'start_';
    } else {
      if (end_marker) {
        end_marker.setMap(null);
      }
      icon = icon_end;
      title = 'End';
      input += 'end_';
    }
    
    var marker = add_marker({
      position: right_click_location,
      title: title,
      map: output_map,
      icon: icon,
      draggable: true
    }, false, '');
    $(input + 'lat').val(right_click_location.lat().toFixed(7));
    $(input + 'lon').val(right_click_location.lng().toFixed(7));
    output_context.style.visibility = "hidden";
    
    if (start) {
      start_marker = marker;
    } else {
      end_marker = marker;
    }
    
    if (start_marker && end_marker) {
      get_directions(start_marker.getPosition(), end_marker.getPosition());
    }
  } catch (e) {
    log ('make_location error: ' + e);
  }
}

function move_location (start) {
  try {
    var input = '#input_';
    
    if (start) {
      input += 'start_';
    } else {
      input += 'end_';
    }
    
    if ($.isNumeric($(input + 'lat').val()) && $.isNumeric($(input + 'lon').val())) {
      if ((start && start_marker) || end_marker) {
        if (start) {
          start_marker.setPosition (new google.maps.LatLng($(input + 'lat').val(), $(input + 'lon').val()));
        } else {
          end_marker.setPosition (new google.maps.LatLng($(input + 'lat').val(), $(input + 'lon').val()));
        }
      } else {
        if (start) {
          start_marker = add_marker({
            position: new google.maps.LatLng($(input + 'lat').val(), $(input + 'lon').val()),
            title: "Start",
            map: output_map,
            icon: icon_start
          }, false, '');
        } else {
          end_marker = add_marker({
            position: new google.maps.LatLng($(input + 'lat').val(), $(input + 'lon').val()),
            title: "End",
            map: output_map,
            icon: icon_end
          }, false, '');
        }
      }
      
      if (start_marker && end_marker) {
        get_directions (start_marker.getPosition(), end_marker.getPosition());
      }
    }
  } catch (e) {
    log ('move_location error: ' + e);
  }
}

function getCanvasXY(currentLatLng) {
  var scale = Math.pow(2, output_map.getZoom());
  var nw = new google.maps.LatLng(output_map.getBounds().getNorthEast().lat(), output_map.getBounds().getSouthWest().lng());
  var worldCoordinateNW = output_map.getProjection().fromLatLngToPoint(nw);
  var worldCoordinate = output_map.getProjection().fromLatLngToPoint(currentLatLng);
  var currentLatLngOffset = new google.maps.Point(
  Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale), Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale));
  return currentLatLngOffset;
}
