var session;
var settings;
var osm_url = '' + window.location;
var sharing_url = '' + window.location;

$(document).ready(function() {
  try {
    if (validate_session()) {
      osm_url = osm_url.substr(0, osm_url.lastIndexOf('/')+1) + config.url_api + config.url_osm_tracking;
      sharing_url = sharing_url.substr(0, sharing_url.lastIndexOf('/')+1) + config.url_location;
      
      $('#div_navbar').html(html_navbar);
      get_settings();
    } else {
      window.location.href = "login.html";
    }
  }
  catch (e) {
    log ("settings error: " + e);
  }
});

function get_settings() {
  try {
    $.ajax({
      type: "GET",
      url: config.url_api,
      async: false,
      cache: false,
      beforeSend: function(xhr) {
        xhr.setRequestHeader('action', 'get_settings');
        var ts = new Date().getTime();
        xhr.setRequestHeader('time_id', ts);
      },
      error: function(req, text, status) {
        log ('get_settings http error: ' + text);
      },
      success: function (resp) {
        if (resp.error) {
          log ('get_settings response error: ' + resp.error);
        } else {
          settings = resp.details;
          $('#input_username').val(settings.username);
          if (settings.position_enabled == '1') {
            $('#output_tracking_enabled').prop('class', 'alert alert-success');
            $('#output_tracking_enabled').html('Tracking is enabled');
            $('#button_enable_tracking').prop('class', 'btn btn-danger');
            enable_tracking();
            $('#button_enable_tracking').text('Disable');
          } else {
            $('#output_tracking_enabled').prop('class', 'alert alert-danger');
            $('#output_tracking_enabled').html('Tracking is disabled');
            $('#button_enable_tracking').prop('class', 'btn btn-success');
            $('#button_enable_tracking').text('Enable');
          }
        }
      }
    });
  } catch (e) {
    log ('get_settings error: ' + e);
  }
}

function enable_tracking() {
  try {
    if (/Enable/.test($('#button_enable_tracking').text())) {
      $('#input_sharing_url').val('');
      settings.position_enabled = 1;
      if (settings.osm_key) {
        $('#input_osm_url').val(osm_url + settings.osm_key);
        $('#input_sharing_url').val(sharing_url + $.cookie(config.cookie).split(':')[0]);
      } else {
        $.ajax({
          type: "GET",
          url: config.url_api,
          async: false,
          cache: false,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('action', 'new_osm_key');
          },
          error: function (req, text, status) {
            log ('enable_tracking http error: ' + text);
          },
          success: function (resp) {
            if (resp.error) {
              log ('enable_tracking response error: ' + resp.error);
            } else {
              settings.osm_key = resp.details;
            }
          }
        });
      }
      $.ajax({
        type: "GET",
        url: config.url_api,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('action', 'enable_tracking');
          xhr.setRequestHeader('enabled', '1');
        },
        error: function (req, text, status) {
          log ('enable_tracking http error: ' + text);
        },
        success: function (resp) {
          if (resp.error) {
            log ('enable_tracking response error: ' + resp.error);
          } else {
            $('#output_tracking_enabled').prop('class', 'alert alert-success');
            $('#output_tracking_enabled').html('Tracking is enabled');
            $('#button_enable_tracking').prop('class', 'btn btn-danger');
            $('#button_enable_tracking').text('Disable');
            $('#input_osm_url').val(osm_url + settings.osm_key);
            $('#input_sharing_url').val(sharing_url + $.cookie(config.cookie).split(':')[0]);
          }
        }
      });
    } else {
      $.ajax({
        type: "GET",
        url: config.url_api,
        async: false,
        cache: false,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('action', 'enable_tracking');
          xhr.setRequestHeader('enabled', '0');
        },
        error: function (req, text, status) {
          log ('enable_tracking http error: ' + text);
        },
        success: function (resp) {
          if (resp.error) {
            log ('enable_tracking response error: ' + resp.error);
          } else {
            $('#output_tracking_enabled').prop('class', 'alert alert-danger');
            $('#output_tracking_enabled').html('Tracking is disabled');
            $('#button_enable_tracking').prop('class', 'btn btn-success');
            $('#button_enable_tracking').text('Enable');
            $('#input_osm_url').val('');
            $('#input_sharing_url').val('');
          }
        }
      });
    }
  } catch (e) {
    log ('enable_tracking error: ' + e);
  }
}

function save_settings() {
  try {
    if (/glyphicon-ok/.test($('#input_username_icon').attr('class'))) {
      if (/glyphicon-ok/.test($('#input_username_icon').attr('class'))) {
        $.ajax({
          type: "GET",
          url: config.url_api,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('action', 'save_settings');
            xhr.setRequestHeader('username', $('#input_username').val());
            xhr.setRequestHeader('password', $.base64.encode($('#input_password').val()));
          },
          error: function (req, text, status) {
            log ('save_settings http error: ' + text);
          },
          success: function (resp) {
            if (resp.error) {
              log ('save_settings response error: ' + resp.error);
            } else {
              alert ('Settings saved');
            }
          }
        });
      } else {
        log_user_error('The passwords don\'t match');
      }
    } else {
      log_user_error('That username is already taken 1');
    }
  } catch (e) {
    log ('save_settings error: ' + e);
  }
}
