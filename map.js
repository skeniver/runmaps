var debug = 1;
var map_id;

var distance_unit = ' km';
var pace_unit = ' min/km';
var speed_unit = ' km/h';
var zoom_level = 10;

var route = new google.maps.Polyline({
  path: [],
  clickable: false,
  strokeColor: '#0000FF',
  strokeWeight: 3
});

var output_map;
var directions_service = new google.maps.DirectionsService();
var info_window = new google.maps.InfoWindow();

var map;
var route_start;
var route_end;
var address_start;
var address_end;

var current_latlng;
var current_address;

var distance_unit = '';
var pace_unit = '';
var speed_unit = '';

window.addEventListener('unload', function() {
  
}, false);

$(document).ready(function() {
  try {
    if (validate_session()) {
      $('#div_navbar').html(html_navbar);
    }
    
    $('#progress_status').show();
    $('#progress_bar').progressBar(10);
    
    map_id = $.url_param('map_id');
    if (map_id && map_id != 'null') {
      google.maps.event.addDomListener(window, 'load', initialise());
    } else {
      window.location.href = "home.html";
    }
    return false;
  } catch (e) {
    log ("Map error: " + e);
  }
});

function initialise() {
// initialise the google map
  try {
    var properties = {
      center: new google.maps.LatLng(0,0),
      zoom: 2,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    output_map = new google.maps.Map(document.getElementById("output_map"), properties);
    
    google.maps.event.addListenerOnce(output_map, 'idle', function() {
      make_map();
    });
  } catch (e) {
    log ("Map initialise error: " + e);
  }
}

function go_home() {
  window.location.href = "home.html";
}

function make_map() {
  try {
    $('#progress_bar').progressBar(20);
    $.ajax({
      type: "GET",
      url: config.url_api,
      async: false,
      cache: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('action', 'get_map');
        xhr.setRequestHeader('map_id', map_id);
        var ts = new Date().getTime();
        xhr.setRequestHeader('time_id', ts);
      },
      error: function (req, text, status) {
        log ('get_map http error: ' + e);
      },
      success: function (resp) {
        try {
          if (resp.error) {
            if (/0011/.test(resp.error)) {
              $('#progress_status').fadeOut();
              $('#div_col_1').delay(1000).fadeOut();
              $('#div_col_2').delay(1000).fadeOut();
              log_user_error('There is no map with that ID');
              setTimeout(go_home, 5000);
            } else {
              log ('get_map data error: ' + resp.error);
            }
          } else {
            map = resp.details;
            if (map.metric == 0) {
              distance_unit = ' mi';
              pace_unit = ' min/mi';
              speed_unit = ' mph';
            } else {
              distance_unit = ' km';
              pace_unit = ' min/km';
              speed_unit = ' kph';
            }
            
            $('#map_name').html('<b>Map: </b>' + map.name + '');
            
            draw_route();
          }
        } catch (e) {
          log ('make_map success error: ' + e);
        }
      }
    });
  } catch (e) {
    log ('make_map error: ' + e);
  }
}

function draw_route() {
  try {
    $('#progress_bar').progressBar(30);
    
    route_start = new google.maps.LatLng(map.start_latitude, map.start_longitude);
    route_end = new google.maps.LatLng(map.end_latitude, map.end_longitude);
    address_start = get_place_name(route_start, 'output_start_address', 'Started at: ', '');
    address_end = get_place_name(route_end, 'output_end_address', 'Finishing at: ', '');
    
    var travel_mode = google.maps.DirectionsTravelMode.WALKING;
    switch (map.travel_mode) {
      case 2:
        travel_mode = google.maps.DirectionsTravelMode.BICYCLING;
        break;
      case 3:
        travel_mode = google.maps.DirectionsTravelMode.DRIVING;
        break;
    }
    
    ///  Retrieve the polyline from the directions, to get and calculate values with
    directions_service.route(
      {
        origin: route_start,
        destination: route_end,
        travelMode: travel_mode
      },
      function (resp, status) {
        try {
          if (status == google.maps.DirectionsStatus.OK) {
            /// Create a polyline for the routes from the route given in the directions
            $(resp.routes[0].legs).each(function(index, legs) {
              $(legs.steps).each(function(index, steps) {
                $(steps.path).each(function(index, path) {
                  route.getPath().push(path);
                });
              });
            });
            route.setMap(output_map);
            route_distance = get_line_distance(route);
            $('#output_map_details').html(
              '<hr>Began on: ' + (new Date(map.start_date*1000).strDate('dd-mon-yyyy'))
              + '</br>Total distance: ' + route_distance.convertDistance('m', distance_unit) + distance_unit
            );
            
            /// Extend the bounds of the map to fit the route
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(route_start);
            bounds.extend(route_end);
            output_map.fitBounds(bounds);
            place_markers();
          } else {
            log ('draw_route request error: ' + status);
          }
        } catch (e) {
          log ('draw_route directions error: ' + e);
        }
      }
    );
  } catch (e) {
    log ('draw_route error: ' + e);
  }
}

function get_place_name(pos, id, prefix, suffix) {
  try {
    if (pos) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({'latLng': pos}, function (results, status) {
        try {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              var addr = results[1].formatted_address;
              var addr1 = addr.substr(0, addr.indexOf(','));
              var addr2 = addr.substr(addr.lastIndexOf(','));
              $('#' + id).html(prefix + addr1 + addr2 + suffix);
            }
          } else {
            log ('get_place_name http error: ' + status);
          }
        } catch (e) {
          log ('get_place_name request error: ' + e);
        }
      });
    } else {
      $('#' + id).html(prefix + 'No position given' + suffix);
    }
  } catch (e) {
    log ('get_place_name error: ' + e);
  }
}

function place_markers() {
  try {
    $('#progress_bar').progressBar(40);
    
    /// Add the start marker
    add_marker (
      {
        position: route_start,
        title: "Start",
        map: output_map,
        icon: icon_start,
        animation: google.maps.Animation.DROP
      },
      false,
      ''
    );
    
    /// if the run begins in the future, only place an end marker
    if (map.start_date > (new Date).getTime() / 1000) {
      add_marker (
        {
          position: route_end,
          title: 'Finish',
          map: output_map,
          icon: icon_end,
          animation: google.maps.Animation.DROP
        },
        false,
        ''
      );
      $('#progress_bar').progressBar(100);
      $('#progress_div').hide();
    } else {
      /// Get the users RK data and place their markers
      $.ajax({
        type: "GET",
        async: false,
        cache: false,
        url: config.url_api,
        beforeSend: function (xhr) {
          xhr.setRequestHeader ('action', 'get_runkeeper_data');
          xhr.setRequestHeader ('map_id', map.id);
          var ts = new Date().getTime();
          xhr.setRequestHeader ('time_id', ts);
        },
        error: function (req, text, status) {
          log ('place_markers http error: ' + text);
        },
        success: function (resp) {
          if (resp.error) {
            log ('place_markers response error: ' + resp.error);
          } else {
            // Get the display version of the route distance (miles/km)
            var route_distance_display = route_distance.convertDistance('m', distance_unit);
            var completed_distance_display = 0;
            
            // If the total distance of the users activities is greater than the total distance of the route, the user has finished
            if (resp.total_distance > route_distance) {
              // - Make the current latlng the end point
              // - Make the completed distance equal to the route distance
              // - Display the finish marker
              current_latlng = get_point_at_distance(route, route_distance);
              completed_distance_display = route_distance_display;
            } else {
              // - Make the current latlng the total activity distance
              // - Get the address of the current latlng
              // - Make the completed distance equal to the total activity distance
              current_latlng = get_point_at_distance(route, resp.total_distance);
              get_place_name (current_latlng, 'output_current_position', 'Current position: ', '');
              completed_distance_display = parseFloat(resp.total_distance).convertDistance('m', distance_unit);
            }
            
            // Make sure the data is in chronological order for all browsers
            var months = [];
            $.each(resp.months, function (index, value) {
              months.push({index: index, value: value});
            });
            months.sort(function (a, b) { return a.index - b.index; });
            
            // Iterate over the monthly data:
            // - Place a marker for each month
            // - Keep a running total of the distance travelled to mark the next month
            
            var distance_counter = 0;
            var duration_counter = 0;
            
            var annual_distance = 0;
            var annual_duration = 0;
            
            var percentage = 30;
            var last_month;
            var last_month_index;
            var last_activity_date = 0;
            var last_date;
            
            $.each (months, function (index, month) {
              try {
                var breaker = false;
                percentage += 60 / months.length;
                $('#progress_bar').progressBar(percentage);
                var marker_options = {
                  map: output_map
                };
                var marker_text = '';
                
                // If the distance travelled exceeds the route distance, iterate over each activity to get the stats for the final marker
                if (distance_counter + month.value.distance >= route_distance) {
                  try {
                    var month_distance_counter = 0;
                    var month_duration_counter = 0;
                    var distance_per_day = 0;
                    
                    var activities = [];
                    $.each(month.value.activities, function (index, value) {
                      activities.push({index: index, value: value});
                    });
                    activities.sort (function (a, b) { return a.index - b.index; });
                    
                    $.each (activities, function (index, activity) {
                      month_distance_counter += activity.value.distance;
                      month_duration_counter += activity.value.duration;
                      annual_distance += activity.value.distance;
                      distance_counter += activity.value.distance;
                      annual_duration += activity.value.duration;
                      duration_counter += activity.value.duration;
                      
                      if (distance_counter >= route_distance) {
                        // The current activity goes over the route distance
                        distance_per_day = (month_distance_counter / new Date(activity.index*1000).getDate()).convertDistance('m', distance_unit);
                        last_month = month.value;
                        last_month.distance = month_distance_counter;
                        last_month.duration = month_duration_counter;
                        last_month_index = index;
                        last_date = new Date(activity.index*1000);
                        return false;
                      }
                    });
                    
                    marker_options['position'] = route_end;
                    marker_options['icon'] = icon_current;
                    
                    marker_text = '<div style="width: 300px;">';
                    marker_text += '<table class="table">';
                    marker_text += '<tr><th colspan="2">' + month.value.name + '</th></tr>';
                    marker_text += '<tr><td>Distance</td><td>' + month_distance_counter.convertDistance('m', distance_unit) + distance_unit + '</td></tr>';
                    marker_text += '<tr><td>Time taken</td><td>' + month_duration_counter.toHours() + '</td></tr>';
                    marker_text += '<tr><td>Average pace</td><td>' + calculate_pace(month_distance_counter, month_duration_counter, map.metric) + pace_unit  + '</td></tr>';
                    marker_text += '<tr><td>Average speed</td><td>' + calculate_speed(month_distance_counter, month_duration_counter, map.metric) + speed_unit  + '</td></tr>';
                    marker_text += '<tr><td>Ground distance per day</td><td>' + distance_per_day + distance_unit  + '</td></tr>';
                    marker_text += '</table>';
                    marker_text += '</div>';
                    
                    breaker = true;
                  } catch (e) {
                    log ('place_markers iteration error 1: ' + e);
                  }
                } else { // distance_counter + month.value.distance < route_distance
                  try {
                    // Add the current months distance & duration to the total counters
                    distance_counter += month.value.distance;
                    duration_counter += month.value.duration;
                    // Add the current months distance & duration to the annual counters
                    annual_distance += month.value.distance;
                    annual_duration += month.value.duration;
                    
                    // Set the markers position
                    marker_options['position'] = get_point_at_distance(route, distance_counter);
                    
                    var annual_stats = '';
                    
                    // If the month is December (12), make the marker an annual one
                    if (month.index.substr(4) == '12') {
                      marker_options['icon'] = icon_year;
                      
                      var first_of_year = new Date(month.index.substr(0,4), 0, 1, 0, 0, 0, 0);
                      var last_of_year = new Date(month.index.substr(0,4), 11, 31, 0, 0, 0, 0);
                      var annual_distance_per_day = '';
                      if (new Date(month.index.substr(0,4), 1, 1).getFullYear() == new Date(map.start_date*1000).getFullYear()) {
                        var days = last_of_year.strDate('doy') - new Date(map.start_date*1000).strDate('doy');
                        annual_distance_per_day = (annual_distance/days).convertDistance('m', distance_unit);
                      }
                      annual_stats = '<table class="table">';
                      annual_stats += '<tr><th colspan="2"><b>' + month.index.substr(0,4) + '</b></th></tr>';
                      annual_stats += '<tr><td>Distance</td><td>' + annual_distance.convertDistance('m', distance_unit) + distance_unit + '</td></tr>';
                      annual_stats += '<tr><td>Time taken</td><td>' + annual_duration.toHours() + '</td></tr>';
                      annual_stats += '<tr><td>Average pace</td><td>' + calculate_pace(annual_distance, annual_duration, map.metric) + pace_unit + '</td></tr>';
                      annual_stats += '<tr><td>Average speed</td><td>' + calculate_speed(annual_distance, annual_duration, map.metric) + speed_unit + '</td></tr>';
                      annual_stats += '<tr><td>Ground distance per day</td><td>' + annual_distance_per_day + distance_unit + '</td></tr>';
                      annual_stats += '</table>';
                      
                      annual_distance = 0;
                      annual_duration = 0;
                    }
                    // If the year and month are the current year/month, make the marker a current one
                    else if (new Date().getFullYear() == month.index.substr(0,4) && new Date().getMonth()+1 == month.index.substr(4)) {
                      marker_options['icon'] = icon_current;
                    }
                    // Otherwise make it an ordinary one
                    else {
                      marker_options['icon'] = icon_month;
                    }
                    
                    // Calculate how many days in the month, to work out the Distance Per Day
                    // The first month must take into account if the map start date isn't the first
                    // The current month must take into account the end
                    var days_in_month = 0;
                    var first_of_month = new Date(month.index.substring(0,4), month.index.substring(4)-1, 1, 0, 0, 0, 0);
                    var last_of_month = new Date(month.index.substring(0,4), month.index.substring(4)-1, 1, 0, 0, 0, 0);
                    last_of_month.setMonth(last_of_month.getMonth()+1);
                    last_of_month.setDate(last_of_month.getDate()-1);
                    var distance_per_day = '';
                    
                    if (new Date(map.start_date*1000).getMonth()+1 == month.index.substr(4) && new Date(map.start_date*1000).getFullYear() == month.index.substr(0,4)) {
                      // This is the first month in the map
                      var days = last_of_month.getDate() - new Date(map.start_date*1000).getDate() + 1;
                      distance_per_day = (month.value.distance / days).convertDistance('m', distance_unit);
                    } else if (new Date().getMonth()+1 == month.index.substr(4) && new Date().getFullYear() == month.index.substr(0,4)) {
                      // This is the current month
                      distance_per_day = (month.value.distance / new Date().getDate()).convertDistance('m', distance_unit);
                    } else {
                      // This is all the other months
                      distance_per_day = (month.value.distance / last_of_month.getDate()).convertDistance('m', distance_unit);
                    }
                    
                    // Set the marker text (beginning is the same for every month)
                    marker_text = '<div style="width: 300px;">';
                    marker_text += '<table class="table">';
                    marker_text += '<tr><th colspan="2">' + month.value.name + '</th></tr>';
                    marker_text += '<tr><td>Distance</td><td>' + month.value.distance.convertDistance('m', distance_unit) + distance_unit + '</td></tr>';
                    marker_text += '<tr><td>Time taken</td><td>' + month.value.duration.toHours() + '</td></tr>';
                    marker_text += '<tr><td>Average pace</td><td>' + calculate_pace(month.value.distance, month.value.duration, map.metric) + pace_unit  + '</td></tr>';
                    marker_text += '<tr><td>Average speed</td><td>' + calculate_speed(month.value.distance, month.value.duration, map.metric) + speed_unit  + '</td></tr>';
                    marker_text += '<tr><td>Ground distance per day</td><td>' + distance_per_day + distance_unit  + '</td></tr>';
                    marker_text += '</table>';
                    marker_text += annual_stats;
                    marker_text += '</div>';
                  } catch (e) {
                    log ('place_markers iteration error 2: ' + e);
                  }
                } // END IF distance_counter + month.value.distance >= route_distance
                add_marker(
                  marker_options,
                  marker_text
                );
                
                last_month = month.value;
                last_month_index = month.index;
                $.each(month.value.activities, function (index, value) {
                  last_activity_date = (index > last_activity_date) ? index : last_activity_date;
                });
                
                if (breaker) {
                  return false;
                }
              } catch (e) {
                log ('place_markers iteration error: ' + e);
              }
            }); // END WHILE
            
            var date = (resp.total_distance > route_distance) ? new Date(last_activity_date*1000) : new Date();
            
            var distance_per_day = (last_month.distance / date.getDate()).convertDistance('m', distance_unit);
            var stats = '<hr>';
            stats += '<div class="page-header"><h4>Performance stats</br><small>This month</small></h4></div>';
            stats += '<table>';
            stats += '<tr><td>Distance</th><td>' + (last_month.distance).convertDistance('m', distance_unit) + distance_unit  + '</td></tr>';
            stats += '<tr><td>Time taken</th><td>' + last_month.duration.toHours()  + '</td></tr>';
            stats += '<tr><td>Average pace</th><td>' + calculate_pace(last_month.distance, last_month.duration, map.metric) + pace_unit  + '</td></tr>';
            stats += '<tr><td>Average speed</th><td>' + calculate_speed(last_month.distance, last_month.duration, map.metric) + speed_unit  + '</td></tr>';
            stats += '<tr><td>Ground distance per day</th><td>' + distance_per_day + distance_unit  + '</td></tr>';
            stats += '</table>';
            
            if (last_date) {
              date = last_date;
            } else {
              date = new Date();
            }
            distance_per_day = (annual_distance / date.strDate('doy')).convertDistance('m', distance_unit);
            stats += '<div class="page-header"><h4><small>This year</small></h4></div>';
            stats += '<table>';
            stats += '<tr><td>Distance</th><td>' + annual_distance.convertDistance('m', distance_unit) + distance_unit  + '</td></tr>';
            stats += '<tr><td>Time taken</th><td>' + annual_duration.toHours()  + '</td></tr>';
            stats += '<tr><td>Average pace</th><td>' + calculate_pace(annual_distance, annual_duration, map.metric) + pace_unit  + '</td></tr>';
            stats += '<tr><td>Average speed</th><td>' + calculate_speed(annual_distance, annual_duration, map.metric) + speed_unit  + '</td></tr>';
            stats += '<tr><td>Ground distance per day</th><td>' + distance_per_day + distance_unit  + '</td></tr>';
            stats += '</table>';
            
            if (!last_date) {
              last_date = new Date();
            }
            distance_per_day = (resp.total_distance / ((last_date.getTime() - new Date(map.start_date*1000).getTime()) / 86400000)).convertDistance('m', distance_unit);
            stats += '<div class="page-header"><h4><small>Total</small></h4></div>';
            stats += '<table>';
            stats += '<tr><td>Distance</th><td>' + distance_counter.convertDistance('m', distance_unit) + distance_unit  + '</td></tr>';
            stats += '<tr><td>Time taken</th><td>' + duration_counter.toHours()  + '</td></tr>';
            stats += '<tr><td>Average pace</th><td>' + calculate_pace(distance_counter, duration_counter, map.metric) + pace_unit  + '</td></tr>';
            stats += '<tr><td>Average speed</th><td>' + calculate_speed(distance_counter, duration_counter, map.metric) + speed_unit  + '</td></tr>';
            stats += '<tr><td>Ground distance per day</th><td>' + distance_per_day + distance_unit  + '</td></tr>';
            stats += '</table>';
            
            $('#output_stats').html(stats);
            
            $('#progress_bar').progressBar(100);
            $('#progress_status').delay(2000).fadeOut();
          }
          resp = null;
        }
      });
    }
  } catch (e) {
    log ('place_markers error: ' + e);
  }
}









function zoom_to_current() {
  try {
    if (current_latlng) {
      google_map.setZoom(zoom_level);
      if (zoom_level == 20) {
        zoom_level == 10;
      } else {
        zoom_level++;
      }
      google_map.panTo(current_latlng);
    } else {
      log ('latlng not defined');
    }
  } catch (e) {
    log ('Zoom to current error: ' + e);
  }
}
