$(document).ready(function() {
  try {
    if (validate_session()) {
      window.location.href = 'home.html';
    } else {
      $('#input_username').focus();
    }
  } catch (e) {
    log ('ready error: ' + e);
  }
});

function show_register(yes) {
  try {
    if (yes) {
      $('#panel_login').fadeOut(500, function() { $('#panel_register').fadeIn(); });
      $('#input_register_username').focus();
    } else {
      $('#panel_register').fadeOut(500, function() { $('#panel_login').fadeIn(); });
    }
  } catch (e) {
    log ('register error: ' + e);
  }
}


function log_in() {
  try {
    var username = $('#input_username').val();
    var password = $('#input_password').val();
    
    if (username && password) {
      $.ajax({
        type: "GET",
        url: config.url_api,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('action', 'login');
          xhr.setRequestHeader('username', username);
          xhr.setRequestHeader('password', $.base64.encode(password));
        },
        error: function(xml, text, status) {
          log ('log_in http error: ' + text);
        },
        success: function(resp) {
          if (resp.error) {
            if (/0005/.test(resp.error)) {
              log_user_error('The username or password are incorrect');
              $('#input_password').focus();
            } else {
              log ('log_in data error: ' + resp.error + ': ' + resp.description);
            }
          } else {
            $.cookie(config.cookie, resp.user_id + ':' + resp.session_key);
            if (resp.runkeeper_token) {
              window.location.href = "home.html";
            } else {
              window.location.href = 'authorise.html';
            }
          }
        }
      });
    } else {
      if (username) {
        $('#input_password').focus();
        log_user_error('You need to enter a password');
      } else {
        $('#input_username').focus();
        log_user_error('You need to enter a username');
      }
    }
  } catch (e) {
    log ('log_in error: ' + e);
  }
}

function register_user() {
log ('a');
  try {
    if (/glyphicon-ok/.test($('#input_username_icon').attr('class'))) {
log ('b');
      if ($('#input_register_password').val() == $('#input_confirm').val()) {
        hide_user_error();
        $.ajax({
          type: "GET",
          url: config.url_api,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('action', 'register');
            xhr.setRequestHeader('username', $('#input_register_username').val());
            xhr.setRequestHeader('password', $.base64.encode($('#input_register_password').val()));
          },
          error: function (req, text, status) {
            log ('register_user http error: ' + text);
          },
          success: function (resp) {
            try {
              if (resp.error) {
                if (/^0000$/.test(resp.error)) {
                  if (/Duplicate entry/.test(resp.description)) {
                    log_user_error('Unfortunately this username is already taken');
                    $('#group_username').attr('class', 'form-group has-error has-feedback');
                    $('#input_username_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
                    $('#input_register_username').focus();
                    unique_username = 0;
                  } else {
                    log ('register_user database error: ' + resp.error + ': ' + resp.description);
                  }
                } else {
                  log ('register_user data error: ' + resp.error + ': ' + resp.description);
                }
              } else {
                $.cookie(config.cookie, resp.user_id + ":" + resp.session_key);
                window.location.href = "authorise.html";
              }
            } catch (e) {
              log ('register_user success error: ' + e);
            }
          }
        });
      } else {
        $('#group_password').attr('class', 'form-group has-error has-feedback');
        $('#input_password_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
        $('#group_confirm').attr('class', 'form-group has-error has-feedback');
        $('#input_confirm_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
        $('#input_register_password').focus();
        log_user_error("Your passwords don't match", 3000);
      }
    } else {
      log_user_error('Unfortunately this username is already taken');
      $('#group_username').attr('class', 'form-group has-error has-feedback');
      $('#input_username_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
      $('#input_register_username').focus();
      unique_username = 0;
    }
  } catch (e) {
    log ('register_user error: ' + e);
  }
}
