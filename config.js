var config = {
  cookie: 'runmaps',
  cookie_user: 'user',
  cookie_session: 'session',
  
  one_mile: 0.621371192,
  
  url_api: 'cgi-bin/api.pl',
  
  
  url_runkeeper: 'cgi-bin/runkeeper.pl',
  url_location: 'location.html?id=',
  url_osm_tracking: '?action=tracking&lat={0}&lon={1}&time={2}&hdop={3}&alt={4}&speed={5}&osm=',
  
  
  months: [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
  ],
  
  content_json: 'application/json; charset=utf-8',
  
  ajax: {
    type: 'GET',
    url: 'cgi-bin/api.pl',
    cache: false,
    dataType: 'json'
  }
}

var html_navbar = "";


/******************

Time and speed calculations

*******************/




/*********************************************************************************
*/
function calculate_speed(metres, sec, metric) {
// calculates speed from metres and seconds
// returns kph or mph
  try {
    var kph = (metres / 1000) / (sec / 3600);
    var mph = ((metres / 1000) * 0.621371192) / (sec / 3600);
    
    var vtr = (metric == 1) ? kph.toFixed(3) : mph.toFixed(2);
    return parseFloat(vtr);
  } catch (e) {
    log ('Speed error: ' + e);
  }
}
function calculate_pace(metres, sec, metric) {
// calculates pace from metres and seconds
// to min/km or min/mi
  try {
    var spk = sec / (metres / 1000);
    var spmi = sec / ((metres / 1000) * 0.621371192);
    
    return (metric == 1) ? spk.toHours() : spmi.toHours();
  } catch (e) {
    log('Pace calculation error: ' + e);
    return 0;
  }
}

function calculate_dpd(metres, days, metric) {
// calculates the actual ground distance covered per day
// from metres and seconds and outputs in km or miles
  try {
    var mpd = metres / days;
    var vtr = (metric == 1) ? (mpd / 1000).toFixed(3) : ((mpd / 1000) * 0.621371192).toFixed(2);
    return parseFloat(vtr);
  } catch (e) {
    log ('Ground distance calculation error: ' + e);
  }
}



//////////////////
//
//  API functions
//
//////////////////
function log_out(all) {
/*************
  If a user choose to log out of all locations, change their session key
  Otherwise, just delete the cookie
**************/
  try {
    if (all) {
      $.ajax({
        type: "GET",
        url: config.url_api,
        cache: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('action', 'log_out');
        },
        error: function (req, text, status) {
          log ('log_out http error: ' + text);
        },
        success: function (resp) {
          if (resp.error) {
            log ('log_out resp error: ' + resp.error);
          } else {
            alert (resp.succes);
//            window.location.href = "login.html";
          }
        }
      });
    } else {
      $.removeCookie(config.cookie);
      window.location.href = "login.html";
    }
  } catch (e) {
    log ('logout error: ' + e);
  }
}
function validate_session() {
/**************
  This function will validate the session key in the users cookie
  If the session key is valid, then the function returns true; if not false
  If the session is valid, but the user has no runkeeper token, it will redirect them to the authorise page

  Updated: 04-05-2014
***************/
  try {
    var valid = false;
    if ($.cookie(config.cookie)) {
      $.ajax ({
        type: "GET",
        url: config.url_api,
        async: false,
        cache: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('action', 'validate_session');
          var ts = new Date().getTime();
          xhr.setRequestHeader('time_id', ts);
        },
        error: function (xml, text, status) {
          log ('validate_session http error: ' + text + ': ' + status);
        },
        success: function (resp) {
          if (resp.error) {
            if (/0001/.test(resp.error)) {
            
            } else {
              if (!/login\.html/.test(window.location)) {
//                window.location.href = 'login.html';
              }
            }
          } else {
            valid = true;
            if (resp.runkeeper_token) {
/*********
              var nav1 = $.html('a', 'RunMaps', { href: 'home.html' });
              var nav2 = $.html('a', 'My location', { href: 'location.html?id=' + data.user_id });
              var nav3 = $.html('a', 'Settings', { href: 'settings.html' });
              var nav4 = $.html('a', 'Wiki', { href: 'https://bitbucket.org/skeniver/runmaps/wiki/Home', target: '_blank' });
              var nav5 = $.html('a', 'Problems?', { href: 'https://bitbucket.org/skeniver/runmaps/issues?status=new&status=open', target: '_blank' });
*/
              
              html_navbar = "\
                <nav class=\"navbar navbar-fixed-top navbar-default\" role=\"navigation\">\
                  <div class=\"container-fluid\">\
                    <ul class=\"nav navbar-nav navbar-left pull-left\">\
                      <li><a href=\"home.html\">RunMaps</a></li>\
                    </ul>\
                    <ul class=\"nav navbar-nav navbar-right pull-right\">\
                      <li class=\"dropdown\">\
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Menu <b class=\"caret\"></b></a>\
                        <ul class=\"dropdown-menu\">\
                          <li><a href=\"location.html?id=" + resp.user_id + "\">My location</a></li>\
                          <li><a href=\"settings.html\">Settings</a></li>\
                          <li><a href=\"https://bitbucket.org/skeniver/runmaps/wiki/Home\" target=\"_blank\">Wiki</a></li>\
                          <li><a href=\"https://bitbucket.org/skeniver/runmaps/issues?status=new&status=open\" target=\"_blank\">Problems?</a></li>\
                          <li class=\"divider\"></li>\
                          <li><a href=\"#\" onclick=\"log_out();\">Logout</a></li>\
                          <li><a href-\"#\" onclick=\"log_out(true);\">Logout everywhere</a></li>\
                        </ul>\
                      </li>\
                    </ul>\
                  </div>\
                </nav>\
              ";
            } else {
              // If the current page id the authorisation page, a redirect would cause a loop
              if (!/authorise\.html/.test(window.location)) {
                window.location.href = 'authorise.html';
              }
            }
          }
        }
      });
    }
    
    return valid;
  } catch (e) {
    log ('validate_session error: ' + e);
  }
}

function is_username_unique(id) {
  try {
    var username = $('#' + id).val();
    if (username) {
      if (/\W/.test(username)) {
        log_user_error('Your username can only contain letters or numbers', 3000);
        $('#' + id).focus();
        $('#group_username').attr('class', 'form-group has-warning has-feedback');
        $('#input_username_icon').attr('class', 'glyphicon glyphicon-warning-sign form-control-feedback');
      } else {
        hide_user_error();
        $.ajax({
          type: "GET",
          url: config.url_api,
          async: false,
          cache: false,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('action', 'verify_username');
            xhr.setRequestHeader('username', username);
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            log ("is_username_unique HTTP error: " + textStatus);
          },
          success: function(data) {
            try {
              if (data.error) {
                if (data.error.match(/0004/i)) {
                  log_user_error('Unfortunately this username is already taken');
                  $('#group_username').attr('class', 'form-group has-error has-feedback');
                  $('#input_username_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
                  $('#' + id).focus();
                  unique_username = 0;
                } else {
                  log ("is_username_unique data error: " + data.error + ': ' + data.description);
                }
              } else {
                $('#group_username').attr('class', 'form-group has-success has-feedback');
                $('#input_username_icon').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
                unique_username = 1;
              }
            } catch (e) {
              log ("is_username_unique ajax error: " + e);
            }
          }
        });
      }
    } else {
      log_user_error('You need to enter a username');
    }
  } catch (e) {
    log ('is_username_unique error: ' + e);
  }
}



////////////////////
// input validation
////////////////////

function valid_date(str) {
  var match = /^(0?[1-9]|[12][0-9]|3[01])[\/\-\\](0?[1-9]|1[012])[\/\-\\]\d{4}$/.exec(str);
  if (match) {
    return true;
  } else {
    return false;
  }
}

function valid_email(email) {
  var email_regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if (email_regex.test(email)) {
    return true;
  } else {
    return false;
  }
}

function compare_passwords(id_pass, id_confirm) {
  try {
    var pass = $('#' + id_pass).val();
    var confirm = $('#' + id_confirm).val();
    
    if (pass && confirm && pass == confirm) {
      $('#group_password').attr('class', 'form-group has-success has-feedback');
      $('#input_password_icon').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
      $('#group_confirm').attr('class', 'form-group has-success has-feedback');
      $('#input_confirm_icon').attr('class', 'glyphicon glyphicon-ok form-control-feedback');
      $('#button_register').removeAttr('disabled');
    } else {
      $('#group_password').attr('class', 'form-group has-error has-feedback');
      $('#input_password_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
      $('#group_confirm').attr('class', 'form-group has-error has-feedback');
      $('#input_confirm_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
      $('#' + id_pass).focus();
    }
  } catch (e) {
    log ('compare_passwords error: ' + e);
  }
}

function check_password(id) {
  try {
    var pass = $('#' + id).val();
    if (!pass) {
      $('#group_password').attr('class', 'form-group has-error has-feedback');
      $('#input_password_icon').attr('class', 'glyphicon glyphicon-remove form-control-feedback');
    }
  } catch (e) {
    log ('check_password error: ' + e);
  }
}



/*
$.fn.progressBar = function(progress) {
  this.attr('style', 'width: ' + progress + '%;');
  return this;
};
*/
