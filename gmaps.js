var current_location_marker;

/******************
  GOOGLE MAPS FUNCTIONS
*************/

function distance_between (p1, p2) {
// returns the distance between two LatLng points
  try {
    var EarthRadiusMeters = 6378137.0; // meters
    var lat1 = p1.lat();
    var lon1 = p1.lng();
    var lat2 = p2.lat();
    var lon2 = p2.lng();
    var dLat = (lat2-lat1) * Math.PI / 180;
    var dLon = (lon2-lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
      Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = EarthRadiusMeters * c;
    return d;
  } catch (e) {
    log ("distance_between error: " + e);
  }
}

function get_line_distance (line) {
// returns the distance of a polyline in metres
  try {
    var dist = 0;
    for (var i=1; i < line.getPath().getLength(); i++) {
      dist += distance_between(line.getPath().getAt(i), line.getPath().getAt(i-1));
    }
    return dist;
  } catch (e) {
    log ("get_line_distance error: " + e);
  }
}

function get_point_at_distance(line, metres) {
// get the point at a particular distance on a google polyline
  // some awkward special cases
  try {
    if (metres == 0) return line.getPath().getAt(0);
    if (metres < 0) return null;
    if (line.getPath().getLength() < 2) return null;
    var dist=0;
    var olddist=0;
    for (var i=1; (i < line.getPath().getLength() && dist < metres); i++) {
      olddist = dist;
      var latlng = line.getPath().getAt(i);
      dist += distance_between(latlng, line.getPath().getAt(i-1));
    }
    if (dist < metres) {
      return null;
    }
    var p1= line.getPath().getAt(i-2);
    var p2= line.getPath().getAt(i-1);
    var m = (metres-olddist)/(dist-olddist);
    return new google.maps.LatLng( p1.lat() + (p2.lat()-p1.lat())*m, p1.lng() + (p2.lng()-p1.lng())*m);
  } catch (e) {
    log ('get_point_at_distance error: ' + e);
  }
}

function get_directions(start, end) {
  try {
    hide_user_error();
    if (start && end) {
      var travel_mode = google.maps.DirectionsTravelMode.WALKING;
      switch ($('#input_travel_mode').val()) {
        case '1':
          travel_mode = google.maps.DirectionsTravelMode.WALKING;
          break
        case '2':
          travel_mode = google.maps.DirectionsTravelMode.BICYCLING;
          break;
        case '3':
          travel_mode = google.maps.DirectionsTravelMode.DRIVING;
          break;
      }
      
      var request = {
        origin: start,
        destination: end,
        travelMode: travel_mode
      };
      directions_service.route(request, function (response, status) {
        try {
          if (status == google.maps.DirectionsStatus.OK) {
            if (start_marker) {
              start_marker.setMap(null);
            }
            if (end_marker) {
              end_marker.setMap(null);
            }
            directions_output.setDirections(response);
          } else {
            if (status == google.maps.DirectionsStatus.ZERO_RESULTS) {
              directions_output.setMap(null);
              start_marker.setMap(output_map);
              end_marker.setMap(output_map);
              log_user_error ('There are no Google directions between those points');
            } else {
              log ('get_directions status error: ' + status);
            }
          }
        } catch (e) {
          log ('get_directions response error: ' + e);
        }
      });
    } else {
      log ('get_directions coords error: start and end position required');
    }
  } catch (e) {
    log ('get_directions error: ' + e);
  }
}

function add_marker(options, info_text) {
  try {
    var marker;
    
    if ('position' in options && options['position']) {
      if ('map' in options && options['map']) {
        marker = new google.maps.Marker(options);
        if (info_text) {
          google.maps.event.addListener(marker, 'click', function() {
            info_window.setContent(info_text);
            info_window.open(output_map, marker);
          });
        }
        if ('draggable' in options && options['draggable']) {
          google.maps.event.addListener(marker, 'dragend', function() {

/// Need to update this to change start/end lat/lng on the editor page
//            $('#input_latitude').val(marker.getPosition().lat());
//            $('#input_longitude').val(marker.getPosition().lng());
          });
        }
        return marker;
      } else {
log ('No map in the marker options');
        return null;
      }
    } else {
log ('No position in the marker options');
      return null;
    }
  } catch (e) {
    log ('add_marker error: ' + e);
  }
}

function show_site_info(site_id) {
  try {
    
  } catch (e) {
    log ('show_site_info error: ' + e);
  }
}

function get_location(caller) {
  try {
    if (caller) {
      $('#' + caller).val("Finding you...");
      $('#' + caller).prop('disabled', 'disabled');
    }
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      current_location_marker = add_marker (
        { position: pos, title: "You", map: output_map, icon: icon_location, draggable: true }
      );
      output_map.setZoom(12);
      output_map.panTo(pos);
      if (caller) {
        $('#' + caller).val("");
        $('#' + caller).removeAttr('disabled');
      }
    });
  } catch (e) {
    log ('get_location error: ' + e);
  }
}

function zoom_here(lat, long) {
  try {
    output_map.setZoom(15);
    output_map.panTo(new google.maps.LatLng(lat, long));
  } catch (e) {
    log ('zoom_here error: ' + e);
  }
}




//////////////////////////////////
///   GOOGLE MAPS ICONS
//////////////////////////////////
var icon_location = {
  scaledSize: new google.maps.Size(40, 40),
  anchor: new google.maps.Point(18,33),
  url: "http://earth.google.com/images/kml-icons/track-directional/track-8.png",
};
var icon_site = {
  scaledSize: new google.maps.Size(40, 40),
  url: "http://maps.google.com/mapfiles/kml/paddle/blu-blank.png"
};
var icon_month = {
  scaledSize: new google.maps.Size(32, 32),
  url: "http://maps.google.com/mapfiles/kml/paddle/pink-blank.png"
};
var icon_year = {
  scaledSize: new google.maps.Size(38, 38),
  url: "http://maps.google.com/mapfiles/kml/paddle/ylw-stars.png"
};
var icon_current = {
  scaledSize: new google.maps.Size(40, 40),
  url: "http://maps.google.com/mapfiles/kml/paddle/grn-blank.png"
};
var icon_start = {
  scaledSize: new google.maps.Size(40, 40),
  url: "http://maps.google.com/mapfiles/kml/paddle/go.png",
};
var icon_end = {
  scaledSize: new google.maps.Size(40, 40),
  url: "http://maps.google.com/mapfiles/kml/paddle/red-square.png"
};
