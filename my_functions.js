/*
These are variable and functions that are used in different pages

This is new
*/

var config = {
  cookie_name: 'runmaps',
  cookie_user: 'user',
  cookie_session: 'session',
  
  one_mile: 0.621371192,

  url_user: 'cgi-bin/user.pl',
  url_runkeeper: 'cgi-bin/runkeeper.pl',
  
  url_location: 'http://runmaps.co.uk/location.html?url=',
  url_osm_tracking: 'http://runmaps.co.uk/cgi-bin/user.pl?action=tracking&lat={0}&lon={1}&time={2}&hdop={3}&alt={4}&speed={5}&osm=',
  url_sharing: 'http://runmaps.co.uk/location.html?url=',
  
  months: [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
  ],

  content_json: 'application/json; charset=utf-8'
}

var icon_start = {
  scaledSize: new google.maps.Size(40, 40),
  url: "http://maps.google.com/mapfiles/kml/paddle/go.png",
};
var icon_end = {
  scaledSize: new google.maps.Size(40, 40),
  url: "http://maps.google.com/mapfiles/kml/paddle/red-square.png"
};
var icon_month = {
  scaledSize: new google.maps.Size(32, 32),
  url: "http://maps.google.com/mapfiles/kml/paddle/pink-blank.png"
};
var icon_year = {
  scaledSize: new google.maps.Size(38, 38),
  url: "http://maps.google.com/mapfiles/kml/paddle/ylw-stars.png"
};
var icon_current = {
  scaledSize: new google.maps.Size(40, 40),
  url: "http://maps.google.com/mapfiles/kml/paddle/grn-blank.png"
//  anchor: new google.maps.Point(20,60)
};




////////////////////
// input validation
////////////////////

function valid_date(str) {
  var match = /^(0?[1-9]|[12][0-9]|3[01])[\/\-\\](0?[1-9]|1[012])[\/\-\\]\d{4}$/.exec(str);
  if (match) {
    return true;
  } else {
    return false;
  }
}

function valid_email(email) {
  try {
    var email_regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (email_regex.test(email)) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    log ('Email validation error: ' + e);
    return false;
  }
}



//////////////////
// time/distance/speed/date calculations
//////////////////
function days_between_dates(start_date, end_date) {
  return Math.ceil((end_date.getTime() - start_date.getTime()) / (1000 * 60 * 60 * 24) - 1)
}

function calculate_dpd(metres, days, metric) {
// calculates the actual ground distance covered per day
// from metres and seconds and outputs in km or miles
  try {
    var mpd = metres / days;
    var vtr = (metric == 1) ? (mpd / 1000).toFixed(3) : ((mpd / 1000) * 0.621371192).toFixed(2);
    return parseFloat(vtr);
  } catch (e) {
    log ('Ground distance calculation error: ' + e);
  }
}

function convert_epoch_to_str_datetime (epoch) {
// converts an epoch to dd/mm/yyyy hh:mm:ss
  try {
    var date = new Date (parseInt(epoch));
    var hours = (date.getHours() < 10) ? '0' + date.getHours() : date.getHours();
    var minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes();
    var seconds = (date.getSeconds() < 10) ? '0' + date.getSeconds() : date.getSeconds();
    return date.getDate() + " " + config.months[date.getMonth()] + ' ' + date.getFullYear()
      + ' ' + hours + ':' + minutes + ':' + seconds + ' (GMT+' + date.getTimezoneOffset() + ')';
  } catch (e) {
    log ('convert_epoch_to_str_datetime error: ' + e);
  }
}

function convert_epoch_to_str_date(epoch) {
// converts spoch milliseconds to dd/mm/yyyy
  try {
    var date = new Date(1000*epoch);
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
  } catch (e) {
    log ('Convert epoch error: ' + e);
  }
}

function calculate_speed(metres, sec, metric) {
// calculates speed from metres and seconds
// returns kph or mph
  try {
    var kph = (metres / 1000) / (sec / 3600);
    var mph = ((metres / 1000) * 0.621371192) / (sec / 3600);
    
    var vtr = (metric == 1) ? kph.toFixed(3) : mph.toFixed(2);
    return parseFloat(vtr);
  } catch (e) {
    log ('Speed error: ' + e);
  }
}

function calculate_pace(metres, sec, metric) {
// calculates pace from metres and seconds
// to min/km or min/mi
  try {
    var spk = sec / (metres / 1000);
    var spmi = sec / ((metres / 1000) * 0.621371192);
    
    return (metric == 1) ? convert_seconds(spk) : convert_seconds(spmi);
  } catch (e) {
    log('Pace calculation error: ' + e);
    return 0;
  }
}

function convert_seconds(seconds) {
// converts seconds to HH:mm:ss
  try {
    seconds = parseFloat(seconds).toFixed();
    hours = parseInt(seconds / 3600);
    minutes = parseInt(seconds / 60) % 60;
    seconds = seconds % 60;
    
    return (hours < 10 ? "0" + hours : hours)
      + ":" + (minutes < 10 ? "0" + minutes : minutes)
      + ":" + (seconds  < 10 ? "0" + seconds : seconds);
  } catch (e) {
    log ('Convert seconds error: ' + e);
  }
}

function display_distance(metres, metric) {
// converts metres to km or miles
  try {
    var dist;
    if (metric == 1) {
      dist = (metres / 1000).toFixed(3);
    } else {
      dist = ((metres / 1000) * 0.621371192).toFixed(2) ;
    }
    return parseFloat(dist);
  } catch (e) {
    log ('Convert metres error: ' + e);
  }
}







//////////////////
// google maps things
//////////////////
function add_marker(marker_options, clickable, info_content) {
  try {
    var marker;
    
    if ("position" in marker_options && marker_options['position']) {
      if ('map' in marker_options && marker_options['map']) {
        marker = new google.maps.Marker(marker_options);
        if (clickable) {
          google.maps.event.addListener(marker, 'click', function() {
            info_window.setContent(info_content);
            info_window.open(google_map, marker);
          });
        }
        return marker;
      } else {
        return null;
      }
    } else {
      return null;
    }
  } catch (e) {
    log ("Add marker error: " + e); 
  }
}

function distance_between (p1, p2) {
// returns the distance between two LatLng points
  try {
    var EarthRadiusMeters = 6378137.0; // meters
    var lat1 = p1.lat();
    var lon1 = p1.lng();
    var lat2 = p2.lat();
    var lon2 = p2.lng();
    var dLat = (lat2-lat1) * Math.PI / 180;
    var dLon = (lon2-lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
      Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = EarthRadiusMeters * c;
    return d;
  } catch (e) {
    log ("Distance between error: " + e);
    return null;
  }
}

function get_line_distance (line) {
// returns the distance of a polyline in metres
  try {
    var dist = 0;
    for (var i=1; i < line.getPath().getLength(); i++) {
      dist += distance_between(line.getPath().getAt(i), line.getPath().getAt(i-1));
    }
    return dist;
  } catch (e) {
    log ("Get line distance error: " + e);
    return null;
  }
}

function get_point_at_distance(line, metres) {
// get the point at a particular distance on a google polyline
  // some awkward special cases
  try {
    if (metres == 0) return line.getPath().getAt(0);
    if (metres < 0) return null;
    if (line.getPath().getLength() < 2) return null;
    var dist=0;
    var olddist=0;
    for (var i=1; (i < line.getPath().getLength() && dist < metres); i++) {
      olddist = dist;
      var latlng = line.getPath().getAt(i);
      dist += distance_between(latlng, line.getPath().getAt(i-1));
    }
    if (dist < metres) {
      return null;
    }
    var p1= line.getPath().getAt(i-2);
    var p2= line.getPath().getAt(i-1);
    var m = (metres-olddist)/(dist-olddist);
    return new google.maps.LatLng( p1.lat() + (p2.lat()-p1.lat())*m, p1.lng() + (p2.lng()-p1.lng())*m);
  } catch (e) {
    $("div#result").append('</br>get point error: ' + e + "</br>");
  }
}





//////////////////
// Control functions
//////////////////
function is_user_elite() {
/* Checks a users RunKeeper status (free or paid account)
Return values:
-1: not checked
 0: free user
 1: elite user
*/
  try {
    var user = get_user_id_from_cookie();
    var is_elite = -1;
    if (user) {
      $.ajax({
        type: "GET",
        url: config.url_runkeeper,
        data: "get=elite&user=" + user,
        async: false,
        error: function(httprequest, text, error) {
          log ("Elite HTTP error: " + text);
        },
        success: function(data) {
          if (data.error) {
            log ("Elite error: " + data.error);
          } else {
            if (/elite/.test(data.success)) {
              is_elite = 1;
            } else {
              is_elite = 0;
            }
          }
        }
      });
    }
  } catch (e) {
    log ("Elite error: " + e);
  }
  return is_elite;
}

function log_user_out() {
// user has clicked a log out button
// try change the session key, but always remove the cookie
  try {
    var user = get_user_id_from_cookie();
    if (user) {
      $.ajax({
        type: "GET",
        url: config.url_user,
        data: "action=logout&user=" + user,
        error: function() {
          $.removeCookie(config.cookie_name);
          window.location.href = "login.html";
        },
        success: function() {
          $.removeCookie(config.cookie_name);
          window.location.href = "login.html";
        }
      });
    }
    $.removeCookie(config.cookie_name);
    window.location.href = "login.html";
  } catch (e) {
    log ("Logout error: " + e);
  }
}

function get_user_id_from_cookie() {
// retrieve the user ID from the cookie
  try {
    var cookie_value = $.cookie(config.cookie_name);
    var user_id;
  
    if (cookie_value) {
      var user = cookie_value.match(/user:\w+/);
      user = user[0];
      user = user.split(":");
    
      user_id = user[1];
    } else {
      user_id = -1;
    }
  
    return user_id;
  } catch (e) {
    log ("Cookie user ID error: " + e);
    return null;
  }
}

function get_session_key_from_cookie() {
  try {
    var cookie_value = $.cookie(config.cookie_name);
    if (cookie_value) {
      var session = cookie_value.match(/session:\w+/);
      session = session[0];
      session = session.split(":");
      return session[1];
    } else {
      return null;
    }
  } catch (e) {
    log ('Cookie session key error: ' + e);
    return null;
  }
}

/********
function is_session_valid() {
Check whether the session key in a cookie is valid
return values are 0 = false; 1 = true:
session valid : authenticated : elite : location enabled


  try {
    var session_key;
    var user;
    var can_login = {'can_login':0};
    if ($.cookie(config.cookie_name)) { // a cookie exists so check for values
      session_key = $.cookie(config.cookie_name).match(/session:\w+/);
      user = $.cookie(config.cookie_name).match(/user:\w+/);
      
      if (user && session_key) { // there needs to be a user id and session key in the cookie
        session_key = session_key[0];
        session_key = session_key.split(":");
        user = user[0];
        user = user.split(":");
        
        var data = "action=validate&user=" + user[1] + "&sessionkey="+session_key[1];
        $.ajax({
          type: "GET",
          url: config.url_user,
          contentType: config.content_json,
          data: data,
          async: false,
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            can_login = '0';
          }, // end of error function
          success: function(data) {
            if (data.error) {
              can_login = {'can_login' : 0};
            }
            else {
              var authed = (data.token) ? '1' : '0';
              can_login = {
                'can_login':1,
                'authenticated':authed,
                'token':data.token,
                'elite':data.elite,
                'username':data.username,
                'location_enabled':data.location_enabled,
                'url_username':data.url_username,
                'osm_key':data.osm_key,
                'url':data.url
              };
            }
          } // end of success function
        }); // end of the ajax
      }  // end of user id and key checking
      else {
        can_login = false;
      }
    } // end of cookie checking
    else {
      can_login = false;
    }
    return can_login;
  } // main try block
  catch (e) {
    log ("mf session validation error: " + e);
    return false;
  } //catch
}
*/


/////////////////
// Error display
/////////////////
/*
function message (error) {
  try {
    $('span#message').html('</br>' + error);
    $('span#message').css('display', 'inline-block');
    $('span#message').show();
  } catch (e) {
    log ('Information error: ' + e);
  }
}
function success (error) {
  try {
    $('span#success').html('</br>' + error);
    $('span#success').css('display', 'inline-block');
    $('span#success').show();
  } catch (e) {
    log ('Success error: ' + e);
  }
}
function user_error (error) {
  try {
    $('span#user_error').html('</br>' + error);
    $('span#user_error').css('display', 'inline-block');
    $('span#user_error').show();
  } catch (e) {
    log ("User error logger: " + e)
  }
}
function user_error_hide() {
  //  $('span#user_error').css('display', 'none');
  $('span#user_error').hide();
}
function log (error) {
  try {
    $('span#error').append('</br>' + (new Date().getTime() / 1000) + " " + error);
    $('span#error').css('display', 'inline-block');
    $('span#error').show();
  } catch (e) {
    alert ("Error logger: " + e)
  }
}
*/
